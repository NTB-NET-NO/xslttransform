VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormReplace 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form2"
   ClientHeight    =   5580
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10215
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5580
   ScaleWidth      =   10215
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox ComboType 
      Height          =   315
      ItemData        =   "FormReplace.frx":0000
      Left            =   120
      List            =   "FormReplace.frx":0013
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton cmdSlett 
      Caption         =   "&Slett"
      Height          =   375
      Left            =   1320
      TabIndex        =   4
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdNy 
      Caption         =   "&Ny"
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   5160
      Width           =   1215
   End
   Begin VB.TextBox txtXPath 
      Height          =   375
      Left            =   2640
      TabIndex        =   1
      Top             =   120
      Width           =   7455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   7440
      TabIndex        =   5
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   8880
      TabIndex        =   6
      Top             =   5160
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid Grid1 
      Height          =   4455
      Left            =   0
      TabIndex        =   2
      Top             =   600
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   7858
      _Version        =   393216
      Cols            =   8
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Label1"
      Height          =   255
      Left            =   1440
      TabIndex        =   7
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "FormReplace"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public lpXchgIndex As Long
Public intRow As Integer
Dim lpStart As Long
Dim recXmlXchg As typXmlXchg
Dim recXmlXchgTom As typXmlXchg

Private Sub cmdAvbryt_Click()
    Unload Me
End Sub

Private Sub cmdNy_Click()
    If Me.ComboType = "" Then
        MsgBox "Du m� oppgi type f�rst!"
        Exit Sub
    End If
    FormReplaceDialog.lpXpathIndex = 0 'Me.Grid1.Text
    FormReplaceDialog.strType = Me.ComboType 'recXmlXchg.strType
    FormReplaceDialog.intRow = Me.Grid1.Row
    FormReplaceDialog.lpXchgIndex = lpXchgIndex
    FormReplaceDialog.Show vbModal
    Me.Grid1.ColSel = 7

End Sub

Private Sub cmdOK_Click()
    If Me.ComboType = "" Then
        MsgBox "Du m� oppgi type f�rst!"
        Exit Sub
    End If
    
    With recXmlXchg
        .strXpath = Me.txtXPath
        .strType = Me.ComboType
        
'        .strType = Me.ComboType
'        .strPath = Me.txtXPath
'        .strFind = Me.txtFind
'        .intHit = Me.txtHit
'        .strCompare = Me.ComboCompare
    End With

    If lpXchgIndex = 0 Then
        LagNy lpXchgIndex
    End If
    
    'arrXmlXchg(lpXchgIndex).strXpath = recXmlXchg.strXpath
    'arrXmlXchg(lpXchgIndex).strXpath = recXmlXchg.strXpath
    arrXmlXchg(lpXchgIndex) = recXmlXchg
    FormXchg.VisEnRad intRow, lpXchgIndex
 
    Unload Me
End Sub

Private Sub ComboType_Validate(Cancel As Boolean)
    If Me.ComboType = "xpath" Then
        Me.Label1.Caption = "XPath:"
    Else
        Me.Label1.Caption = "Tekst:"
    End If
End Sub

Private Sub Form_Load()
    Dim lpEnd As Long
    Dim lpIndex As Long
    Dim intRow As Integer
    
    If lpXchgIndex = 0 Then
        recXmlXchg = recXmlXchgTom
    Else
        recXmlXchg = arrXmlXchg(lpXchgIndex)
    End If
    
    With recXmlXchg
        If .strType = "xpath" Then
            Me.Label1.Caption = "XPath:"
        Else
            Me.Label1.Caption = "Tekst:"
        End If
        lpStart = .lpReplaceStart
        lpEnd = .lpReplaceEnd
        Me.txtXPath = .strXpath
        If .strType <> "" Then
            Me.ComboType = .strType
        End If
    End With
    
    Me.Grid1.Row = 0
    Me.Grid1.ColWidth(0) = 300
    Me.Grid1.ColWidth(1) = 2000
    Me.Grid1.ColWidth(2) = 2000
    Me.Grid1.ColWidth(3) = 2000
    Me.Grid1.ColWidth(4) = 2000
    Me.Grid1.ColWidth(5) = 500
    Me.Grid1.ColWidth(6) = 500
    Me.Grid1.ColWidth(7) = 500
    Me.Grid1.Col = 0
    Me.Grid1.Text = "Nr"
    Me.Grid1.Col = 1
    Me.Grid1.Text = "Find"
    Me.Grid1.Col = 2
    Me.Grid1.Text = "Replace"
    Me.Grid1.Col = 3
    Me.Grid1.Text = "Find2"
    Me.Grid1.Col = 4
    Me.Grid1.Text = "Replace2"
    Me.Grid1.Col = 5
    Me.Grid1.Text = "Start"
    Me.Grid1.Col = 6
    Me.Grid1.Text = "Count"
    Me.Grid1.Col = 7
    Me.Grid1.Text = "Compare"
    
    If lpXchgIndex > 0 Then
        lpIndex = lpStart
        Do
            intRow = intRow + 1
            VisEnRad intRow, lpIndex
        Loop Until lpIndex = 0
    End If
    
    Me.Grid1.Row = 1
    Me.Grid1.Col = 1
    Me.Grid1.ColSel = 7
End Sub

Sub VisEnRad(intRow As Integer, lpIndex As Long)
        If intRow >= Me.Grid1.Rows Then
            Me.Grid1.AddItem ("")
            'intRow = Me.Grid1.Rows - 1
        End If
        Me.Grid1.Row = intRow
        With arrXchgReplace(lpIndex)
            Me.Grid1.RowData(intRow) = lpIndex
            Me.Grid1.Col = 0
            Me.Grid1.Text = Format(intRow, "00")
            Me.Grid1.Col = 1
            Me.Grid1.Text = .strFindShow
            Me.Grid1.Col = 2
            Me.Grid1.Text = .strReplaceShow
            Me.Grid1.Col = 3
            Me.Grid1.Text = .strFind2Show
            Me.Grid1.Col = 4
            Me.Grid1.Text = .strReplace2Show
            Me.Grid1.Col = 5
            Me.Grid1.Text = .intStart
            Me.Grid1.Col = 6
            Me.Grid1.Text = .intCount
            Me.Grid1.Col = 7
            Me.Grid1.Text = .intCompare
            lpIndex = .lpNexNode
        End With

End Sub

Private Sub Grid1_dblClick()
    'FormReplaceDialog.lpXpathIndex = lpStart + Me.Grid1.Row - 1
    If Me.ComboType = "" Then
        MsgBox "Du m� oppgi type f�rst!"
        Exit Sub
    End If
    FormReplaceDialog.lpXpathIndex = Me.Grid1.RowData(Me.Grid1.Row) 'Me.Grid1.Text
    FormReplaceDialog.strType = Me.ComboType 'recXmlXchg.strType
    FormReplaceDialog.intRow = Me.Grid1.Row
    FormReplaceDialog.lpXchgIndex = lpXchgIndex
    FormReplaceDialog.Show vbModal
    Me.Grid1.ColSel = 7
End Sub

Private Sub Grid1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Grid1_dblClick
    End If
End Sub

Private Sub LagNy(lpIndex As Long)
    On Error GoTo Err_Handler
    'Dim lpIndex As Long
    Dim intRow As Integer
    Dim lpPrevNode As Long
        
    lpPrevNode = arrTrans(intJobIndex).lpXchgEnd
    lpIndex = UBound(arrXmlXchg) + 1
    
    arrXmlXchg(lpPrevNode).lpNexNode = lpIndex
    
    ReDim Preserve arrXmlXchg(lpIndex)
    'arrXmlXchg(lpIndex) = recXmlXchg
    arrTrans(intJobIndex).lpXchgEnd = lpIndex

    Exit Sub
Err_Handler:
    MsgBox Err.Description, vbCritical, "Feil"
End Sub

