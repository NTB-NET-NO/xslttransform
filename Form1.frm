VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form Form1 
   ClientHeight    =   8355
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   14460
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8355
   ScaleWidth      =   14460
   StartUpPosition =   3  'Windows Default
   Tag             =   "XSLT Transform - ver.: 1.6.1 - 07.12.2001"
   Visible         =   0   'False
   Begin VB.CommandButton cmdInstillinger 
      Caption         =   "Instillinger"
      Height          =   375
      Left            =   5400
      TabIndex        =   4
      Top             =   0
      Width           =   1095
   End
   Begin VB.CommandButton cmdLagreIni 
      Caption         =   "Lagre IniFil"
      Height          =   375
      Left            =   6840
      TabIndex        =   5
      Top             =   0
      Width           =   1095
   End
   Begin VB.CommandButton cmdFytt 
      Caption         =   "Ned"
      Height          =   375
      Index           =   2
      Left            =   13800
      TabIndex        =   9
      Top             =   5520
      Width           =   615
   End
   Begin VB.CommandButton cmdFytt 
      Caption         =   "Opp"
      Height          =   375
      Index           =   0
      Left            =   13800
      TabIndex        =   8
      Top             =   480
      Width           =   615
   End
   Begin VB.CommandButton cmdStartJobb 
      Caption         =   "Stopp J&obb"
      Height          =   375
      Index           =   0
      Left            =   4080
      TabIndex        =   3
      ToolTipText     =   "Stopp jobb"
      Top             =   0
      Width           =   1095
   End
   Begin VB.CommandButton cmdStartJobb 
      Caption         =   "Start Jo&bb"
      Height          =   375
      Index           =   1
      Left            =   3000
      TabIndex        =   2
      ToolTipText     =   "Start jobb"
      Top             =   0
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid Grid1 
      Height          =   5415
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   13695
      _ExtentX        =   24156
      _ExtentY        =   9551
      _Version        =   393216
      Cols            =   11
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.CommandButton cmdReInit 
      Caption         =   "Initier p� nytt"
      Height          =   375
      Left            =   8040
      TabIndex        =   6
      Top             =   0
      Width           =   1215
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Left            =   13920
      Top             =   0
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "&Start timer"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   1335
   End
   Begin VB.CommandButton cmdPause 
      Caption         =   "&Pause"
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   0
      Width           =   1215
   End
   Begin VB.TextBox txtLog 
      Height          =   2175
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   10
      Top             =   6000
      Width           =   13695
   End
   Begin VB.Menu mnuFil 
      Caption         =   "&Fil"
      Index           =   1
      Begin VB.Menu mnuFilStart 
         Caption         =   "&Start timer"
      End
      Begin VB.Menu mnuFilPause 
         Caption         =   "&Pause timer"
      End
      Begin VB.Menu mnuFilAvslutt 
         Caption         =   "&Avslutt"
      End
   End
   Begin VB.Menu mnuInnstillinger 
      Caption         =   "&Innstillinger"
      Begin VB.Menu mnuInnstillingerEndre 
         Caption         =   "&Endre innstillinger"
      End
      Begin VB.Menu mnuInnstillingerLagre 
         Caption         =   "&Lagre innstillinger"
      End
      Begin VB.Menu mnuInnstillingerHent 
         Caption         =   "&Hent innstillinger"
      End
   End
   Begin VB.Menu mnuJobber 
      Caption         =   "&Jobber"
      Begin VB.Menu mnuJobbNy 
         Caption         =   "&Ny jobb"
      End
      Begin VB.Menu mnuJobRediger 
         Caption         =   "&Rediger jobb"
      End
      Begin VB.Menu mnuJobStart 
         Caption         =   "&Start jobb"
      End
      Begin VB.Menu mnuJobStop 
         Caption         =   "Stopp &jobb"
      End
      Begin VB.Menu mnuJobbSlett 
         Caption         =   "Sl&ett jobb"
      End
   End
   Begin VB.Menu mnuHjelp 
      Caption         =   "&Hjelp"
      Begin VB.Menu mnuHjelpOm 
         Caption         =   "&Om Xslt Transform"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hwnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

Private Sub cmdHentOldIni_Click()
    
'    recGeneral.bTimerEnabled = Timer1.Enabled
'    Timer1.Enabled = False
'    'Me.txtLog = ""
'    SkrivLog "----------- Reinitert programmet ------------------"
'    Old_Init
'    SkrivJobliste
'    Timer1.Enabled = recGeneral.bTimerEnabled
    
End Sub

Private Sub cmdTest_Click()
    TestXmlDom
End Sub

Private Sub Form_Load()
    strCurDir = CurDir
    Ini_File = strCurDir & "\" & INI_FILE_XML
    
    Set FSO = New FileSystemObject
    
    InitXml
    
    Me.Caption = recGeneral.strTitle & " " & Me.Tag

    SkrivLog "----------- Programmet er startet -----------------"
      
    If Not recGeneral.bGui Then
        Me.Hide
    Else
        Me.Show
        FillGridHead
    End If
    
    SkrivJobliste
    
    '*** Start timer
    recGeneral.bTimerEnabled = recGeneral.bTimerEnabledStart
    SkjulKnapper recGeneral.bTimerEnabled
    Timer1.Interval = recGeneral.intInterval
    Timer1.Enabled = recGeneral.bTimerEnabled
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Me.Grid1.Width = (Me.Width) - 1000
    Me.txtLog.Width = (Me.Width) - 1000
    
    Me.cmdFytt(0).Left = (Me.Width) - 800
    Me.cmdFytt(2).Left = (Me.Width) - 800
    
    Me.cmdFytt(2).Top = (Me.Height - 500) / 2 + 100
    
    Me.Grid1.Height = (Me.Height - 500) / 2
    Me.txtLog.Top = (Me.Height - 500) / 2 + 600
    Me.txtLog.Height = (Me.Height - 3800) / 2 + 800

End Sub

Private Sub mnuHelpAbout_Click()
  MsgBox "About Box Code goes here!"
'  frmAbout.Show vbModal
End Sub

Private Sub mnuHelpContents_Click()
  On Error Resume Next
  
  Dim nRet As Integer
  'nRet = OSWinHelp(Me.hwnd, App.HelpFile, 3, 0)
  If Err Then
    MsgBox Err.Description
  End If
End Sub

Private Sub mnuHelpSearch_Click()
  On Error Resume Next
  
  Dim nRet As Integer
    'nRet = OSWinHelp(Me.hwnd, App.HelpFile, 261, 0)
  If Err Then
    MsgBox Err.Description
  End If
End Sub

Private Sub cmdFytt_Click(Index As Integer)
    Dim intPos1 As Integer
    Dim intPos2 As Integer
    Dim Trans1 As typTransRec
    'Dim Trans2 As typTransRec
    Dim intStep As Integer
        
    intStep = Index - 1
    
    intPos1 = Me.Grid1.Row
    intPos2 = Me.Grid1.Row + intStep
    If intPos2 > 0 And intPos2 <= intTransAnt And intPos1 > 0 Then
        Trans1 = arrTrans(intPos2)
        arrTrans(intPos2) = arrTrans(intPos1)
        arrTrans(intPos1) = Trans1
    
        SkrivEnJobb intPos1
        SkrivEnJobb intPos2
        Me.Grid1.Col = 0
        Me.Grid1.ColSel = 9
    End If
End Sub


Private Sub cmdInstillinger_Click()
    FormInstillinger.Show vbModal
End Sub

Private Sub cmdLagreIni_Click()
    LagreIni
End Sub

Private Sub cmdReInit_Click()
    
    recGeneral.bTimerEnabled = Timer1.Enabled
    Timer1.Enabled = False
    'Me.txtLog = ""
    SkrivLog "----------- Reinitert programmet ------------------"
    InitXml
    SkrivJobliste
    Timer1.Enabled = recGeneral.bTimerEnabled
    
End Sub

Private Sub StartJob(intStatus As Integer)
    Dim i As Integer
    Dim intStart As Integer
    Dim intSlutt As Integer
    Dim intColor As Long
      
    If Me.Grid1.Row = 0 Then
        Exit Sub
    ElseIf Me.Grid1.RowSel > Me.Grid1.Row Then
        intStart = Me.Grid1.Row
        intSlutt = Me.Grid1.RowSel
    Else
        intSlutt = Me.Grid1.Row
        intStart = Me.Grid1.RowSel
    End If
    
    For i = intStart To intSlutt
        With arrTrans(i)
            If .strErrStatus = STR_FEIL Then
                MsgBox "Jobb " & Format(i, "00") & " er i feilstatus: " & vbCrLf & .strLastError
            Else
                If intStatus = 1 Then
                    .strStatus = STR_OK
                     intColor = vbGreen
                Else
                    .strStatus = STR_STOPPET
                     intColor = vbWhite
                End If
                    
                Me.Grid1.Row = i
                'Me.Grid1.Col = 0
                'Me.Grid1.CellBackColor = intColor
                    
                Me.Grid1.Col = 1
                Me.Grid1.CellBackColor = intColor
                     
                Me.Grid1.Col = 10
                Me.Grid1.Text = .strStatus
                Me.Grid1.CellBackColor = intColor
                End If
        End With
    Next
    Me.Grid1.Row = intSlutt
    Me.Grid1.Col = 1
    Me.Grid1.RowSel = intStart
    Me.Grid1.ColSel = 9
    Me.Grid1.SetFocus
    
End Sub

Private Sub cmdStartJobb_Click(Index As Integer)
    StartJob Index
End Sub

Private Sub Grid1_dblClick()
    If Not recGeneral.bTimerEnabled Then
        intJobIndex = Me.Grid1.Row
        FormEgenskaper.Show vbModal
    End If
End Sub

Private Sub Grid1_GotFocus()
    If Me.Grid1.Row = 0 Then
        Me.Grid1.Row = 1
    End If
    Me.Grid1.Col = 1
    Me.Grid1.ColSel = 10
End Sub

Private Sub Grid1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Grid1_dblClick
    End If
End Sub

Private Sub Grid1_LostFocus()
    If Me.Grid1.Row = Me.Grid1.RowSel Then
        Me.Grid1.Col = 1
        Me.Grid1.ColSel = 1
    End If
End Sub

Private Sub mnuFilAvslutt_Click()
    Unload Me
End Sub

Private Sub mnuJobbNy_Click()
    intJobIndex = intTransAnt + 1
    FormEgenskaper.Show vbModal

'    Me.Grid1.AddItem Format(Me.Grid1.Rows, "00")
'    Me.Grid1.Col = 1
'    Me.Grid1.ColSel = 9
'    Me.Grid1.Row = Me.Grid1.Rows - 1
''    Grid1_DblClick
End Sub

Private Sub mnuJobbSlett_Click()
    Dim intJobNr As Integer
    Dim i As Integer
    
    intJobNr = Me.Grid1.Row
    
    If intJobNr = 0 Then Exit Sub
    
    With arrTrans(intJobNr)
    
        strMessage = .strNr & ": " & .strText
        If MsgBox("Vil du slette jobb: " & strMessage, vbYesNo Or vbQuestion, "Slett") = vbNo Then
            Exit Sub
        End If
            
        For i = intJobNr To intTransAnt - 1
            arrTrans(i) = arrTrans(i + 1)
            SkrivEnJobb i
        Next
        
        strMessage = strMessage & ": Mapper: " & .strInPath & " -> " & .strOutPath
    End With
    
    Me.Grid1.RemoveItem intTransAnt
    intTransAnt = intTransAnt - 1
    ReDim Preserve arrTrans(1 To intTransAnt)
        
    If intJobNr > intTransAnt Then
        intJobNr = intTransAnt
    End If
        
    Me.Grid1.Row = intJobNr
    Me.Grid1.Col = 1
    Me.Grid1.ColSel = 10
        
    SkrivLog "Slettet jobb nr: " & strMessage
End Sub

Private Sub mnuJobRediger_Click()
    If Me.Grid1.Row > 0 Then
        Grid1_dblClick
    Else
        MsgBox "Du m� velge en jobb f�rst!"
        Me.Grid1.SetFocus
    End If
End Sub

Private Sub Timer1_Timer()
    On Error GoTo Err_Handler
    'recGeneral.bTimerEnabled = True
    SkjulKnapper True

    Timer1.Enabled = False
'    Me.cmdStart.Enabled = False
'    Me.cmdReInit.Enabled = False
    Me.cmdStart.Caption = Time
    DoJobs
    
'    Me.cmdReInit.Enabled = True
    
    If recGeneral.intInterval = 0 Then recGeneral.intInterval = 5000
    Timer1.Interval = recGeneral.intInterval

    Timer1.Enabled = recGeneral.bTimerEnabled
    SkjulKnapper recGeneral.bTimerEnabled
Exit Sub
Err_Handler:
   'SkrivErrLog Err, "Timer1_Timer"
   End
End Sub

Private Sub cmdPause_Click()
    Me.cmdPause.Enabled = False
    If Me.Timer1.Enabled = True Then
        SkjulKnapper False
        Me.Timer1.Enabled = False
    End If
    recGeneral.bTimerEnabled = False
    'Me.Timer1.Enabled = False

End Sub

Private Sub cmdStart_Click()
    recGeneral.bTimerEnabled = True
    Timer1.Interval = 1
    Me.Timer1.Enabled = True
End Sub

Private Sub SkjulKnapper(bEnable As Boolean)
    Me.mnuFilPause.Enabled = bEnable
    Me.cmdPause.Enabled = bEnable
    
    Me.cmdStart.Enabled = Not bEnable
    Me.cmdFytt(0).Enabled = Not bEnable
    Me.cmdFytt(2).Enabled = Not bEnable
    Me.cmdInstillinger.Enabled = Not bEnable
    Me.cmdLagreIni.Enabled = Not bEnable
    Me.cmdReInit.Enabled = Not bEnable
    Me.cmdStartJobb(0).Enabled = Not bEnable
    Me.cmdStartJobb(1).Enabled = Not bEnable
    
    Me.mnuInnstillinger.Enabled = Not bEnable
    Me.mnuJobber.Enabled = Not bEnable
    Me.mnuHjelp.Enabled = Not bEnable
    Me.mnuFilStart.Enabled = Not bEnable
    Me.mnuFilAvslutt.Enabled = Not bEnable
    If Not bEnable Then
        Me.cmdStart.Caption = "&Start timer"
    End If
End Sub

Private Sub DoJobs()
    On Error GoTo Err_Handler
    Dim strInnFile As String
    Dim strPathFile As String
    Dim strOutFile As String
    Dim intJobNr As Integer
    Dim intLines As Integer
    Dim bSkipFile As Boolean
    Dim bTest As Boolean
    
    For intJobNr = 1 To intTransAnt
        With arrTrans(intJobNr)
        If .strStatus = STR_OK And .strErrStatus <> STR_FEIL Then
            
            JobRunning intJobNr, True
            
            DoEvents
            
            strInnFile = Dir(.strInPath & "\" & .strFilter)
            
            If .strErrStatus = STR_VARSEL Then
                .strErrStatus = ""
                SkrivEnJobb intJobNr
            End If
            
            If intJobNr = 1 And strInnFile <> "" Then
                '*** Lar f�rste jobb f� en liten pause etter DIR,
                '*** slik at filer skrevet fra et annet program
                '*** f�r skrevet filen ferdig f�r den blir lest!
                Sleep 4000
            End If
            
            Do Until strInnFile = ""
            '*** For hver input-fil
                strPathFile = .strInPath & "\" & strInnFile
                
                '*** Nullstiller Fil-tekst buffer
                Glob.Type = typEmpty
                Glob.bLoaded = False
                Glob.strText = ""
                Glob.strFileName = ""
                
                '*** Sjekker og eventuelt setter fil-attributt:
                If Not FileArchive(.strArchive, strPathFile) Then
                    GoTo Skip_File
                End If
                
                
                '*** Leser fila inn i strengvariabel: Glob.strText
                '*** XCHG, bytter tegn:
                If .bXchg Then
                    '*** Hoppes over ved type "valg"
                    If .strType <> "valg" Then
                        If LoadFile(intJobNr, strPathFile, Glob, typText) Then
                            DoTxtXchg Glob.strText, intJobNr
                        Else
                            GoTo Skip_File
                        End If
                    End If
                End If
                
                '*** Spesialjobber utenom "trans":
                Select Case .strType
                Case "xmlrtf"
        
                    If Not LoadFile(intJobNr, strPathFile, Glob, typText) Then
                        GoTo Skip_File
                    End If
        
                    If XMLRtfTransform(Glob, strPathFile, bSkipFile) = False Then
                        CopyError intJobNr, strPathFile
                        GoTo Skip_File
                    End If
                    
                    If bSkipFile Then
                        GoTo Skip_File
                    End If

                Case "lyd"
                    If LoadFile(intJobNr, strPathFile, Glob, typXmlDoc) Then
                        SoundFile intJobNr, Glob
                    End If
                    GoTo Skip_File
                Case "valg"
                    Glob.strText = ConvertValg(strPathFile, intJobNr)
                    Glob.Type = typText
                Case "mail"
                    'MailFile intJobNr, strPathFile
                Case Else
                End Select
                
                '*** XCHG, bytter tegn i XML fil i f�lge XPath:
                If .bXmlXchg Then
                    If LoadFile(intJobNr, strPathFile, Glob, typXmlDoc) Then
                        DoXmlXchg .xmlDoc, intJobNr
                    Else
                        GoTo Skip_File
                    End If
                End If
                
                If .strXsltFile <> "" Then
                    If LoadFile(intJobNr, strPathFile, Glob, typXmlDoc) Then
                        .xsltProc.input = .xmlDoc
                        .xsltProc.Transform
                        Glob.strText = .xsltProc.output
                        Glob.Type = typText
                    Else
                        SkrivErr "DoJobs", strMessage, intJobNr, strPathFile
                        GoTo Skip_File
                    End If
                                              
                End If
                    
                    
                '*** XCHG, bytter tegn til slutt "last":
                If .bLastXchg Then
                    If LoadFile(intJobNr, strPathFile, Glob, typText) Then
                        DoTxtXchg Glob.strText, intJobNr, "last"
                    Else
                        SkrivErr "DoJobs", strMessage, intJobNr, strPathFile
                        GoTo Skip_File
                    End If
                    
                End If
                
                If .strFileExt = "" Then
                    strOutFile = GetFileBase(strInnFile, True)
                Else
                    strOutFile = GetFileBase(strInnFile) & "." & .strFileExt
                End If
                
                If .strOutPath <> "" Then
                    If .lpCopyStart > 0 Then
                        xmlCopyFiles strPathFile, strOutFile, intJobNr, Glob
                    ElseIf Glob.bLoaded Then
                        If Glob.Type = typXmlDoc Then
                            Glob.strText = .xmlDoc.xml
                        End If
                        SaveFile .strOutPath & "\" & strOutFile, Glob.strText, intJobNr
                        SkrivLog "JobNr:" & Format(intJobNr, "00") & " " & .strType, strPathFile, .strOutPath
                    Else
                        'SaveFile .strOutPath & "\" & strOutFile, Glob.strText, intJobNr
                        FSO.CopyFile strPathFile, .strOutPath & "\" & strOutFile
                        SkrivLog "JobNr:" & Format(intJobNr, "00") & " " & .strType, strPathFile, .strOutPath
                    End If
                End If

Skip_File:
                DeleteFile intJobNr, strPathFile

                strInnFile = Dir
                DoEvents
            Loop
            JobRunning intJobNr, False
        End If
        End With
    Next
    
Exit Sub
Err_Handler:
    SkrivErr "DoJobs", Err.Description, intJobNr, strPathFile
    arrTrans(intJobNr).strErrStatus = STR_VARSEL
    SkrivEnJobb intJobNr
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer
    If MsgBox("Dette programmet skal normalt v�re aktivt! Vil du virkelig avslutte?", vbYesNo + vbExclamation + vbDefaultButton2, "Avslutt") _
        = vbNo Then
        Cancel = True
    Else
        '*** Frigj�r memory for Objekter:
        Set FSO = Nothing
        For i = 1 To intTransAnt
            With arrTrans(i)
                Set .xmlDoc = Nothing
                Set .xsltProc = Nothing
            End With
        Next
        SkrivLog "----------- Programmet er avsluttet -------------"
    End If

End Sub

Sub FillGridHead()
    Me.Grid1.ColWidth(0) = 250
    Me.Grid1.ColWidth(1) = 700
    Me.Grid1.ColWidth(2) = 2000
    Me.Grid1.ColWidth(3) = 2500
    Me.Grid1.ColWidth(4) = 2500
    Me.Grid1.ColWidth(5) = 1000
    Me.Grid1.ColWidth(6) = 1000
    Me.Grid1.ColWidth(7) = 1000
    Me.Grid1.ColWidth(8) = 600
    Me.Grid1.ColWidth(9) = 600
    Me.Grid1.ColWidth(10) = 700
    
    Me.Grid1.Row = 0
    Me.Grid1.Col = 0
    Me.Grid1.Text = "Nr"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 1
    Me.Grid1.Text = "Jobtype"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 2
    Me.Grid1.Text = "Tekst"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 3
    Me.Grid1.Text = "Inn path"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 4
    Me.Grid1.Text = "Ut path"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 5
    Me.Grid1.Text = "XSLT"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 6
    Me.Grid1.Text = "Xchg"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 7
    Me.Grid1.Text = "Copy"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 8
    Me.Grid1.Text = "Delete"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 9
    Me.Grid1.Text = "Archive"
    'Me.Grid1.CellFontBold = True
    
    Me.Grid1.Col = 10
    Me.Grid1.Text = "Status"
    'Me.Grid1.CellFontBold = True

End Sub

Public Sub SkrivJobliste()
    On Error GoTo Err_Handler
    Dim i As Integer
    
    Me.Grid1.Rows = 1
    
    For i = 1 To intTransAnt
        SkrivEnJobb i
    Next
    Me.Grid1.Row = 0
Exit Sub
Err_Handler:
    'Form1.txtJobListe = Now & ": 'SkrivJobliste' Feilet: " & Err.Number & ": " & Err.Description
    Resume
End Sub

Public Sub SkrivEnJobb(intJobNr As Integer)
    Dim intColor As Long
    Dim strSetStatus As String
    
    If Me.Grid1.Rows <= intJobNr Then
        Me.Grid1.AddItem ""
    End If
    With arrTrans(intJobNr)
        If .strErrStatus <> "" Then
            strSetStatus = .strErrStatus
        Else
            strSetStatus = .strStatus
        End If
        
        Select Case strSetStatus
            Case STR_OK
                intColor = vbGreen
            Case STR_FEIL
                intColor = vbRed
            Case STR_VARSEL
                intColor = vbYellow
            Case Else
                intColor = vbWhite
        End Select
        
        Me.Grid1.Row = intJobNr
        
        Me.Grid1.Col = 0
        Me.Grid1.Text = .strNr
        'Me.Grid1.CellBackColor = intColor
        
        Me.Grid1.Col = 1
        Me.Grid1.Text = .strType
        Me.Grid1.CellBackColor = intColor
        Me.Grid1.CellFontBold = True
        
        Me.Grid1.Col = 2
        Me.Grid1.Text = .strText
        
        Me.Grid1.Col = 3
        Me.Grid1.Text = .strInPath
        
        Me.Grid1.Col = 4
        Me.Grid1.Text = .strOutPath
        
        Me.Grid1.Col = 5
        Me.Grid1.Text = .strXsltFile
        
        Me.Grid1.Col = 6
        Me.Grid1.Text = .strXchgFile
        
        Me.Grid1.Col = 7
        Me.Grid1.Text = .strCopyFile
        
        Me.Grid1.Col = 8
        Me.Grid1.Text = .strDelete & " " & .strDelDays
        
        Me.Grid1.Col = 9
        Me.Grid1.Text = .strArchive
        
        Me.Grid1.Col = 10
        Me.Grid1.Text = strSetStatus
        Me.Grid1.CellBackColor = intColor
        Me.Grid1.CellFontBold = True
    End With
End Sub

Private Sub JobRunning(i As Integer, bRun As Boolean)
    Dim intColor As Long
    Dim intCol As Integer
    
    If bRun = True Then
         intColor = vbGreen
    Else
         intColor = vbWhite
    End If
        
    Me.Grid1.Row = i
    
    'For intCol = 2 To 2
        intCol = 2
        Me.Grid1.Col = intCol
        Me.Grid1.CellBackColor = intColor
    'Next
    'Me.txtLog.SetFocus
End Sub

