VERSION 5.00
Begin VB.Form FormInstillinger 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Instillinger"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7710
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   7710
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame5 
      Caption         =   "Tittel"
      Height          =   1095
      Left            =   120
      TabIndex        =   21
      Top             =   120
      Width           =   7455
      Begin VB.TextBox txtInterval 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   6480
         TabIndex        =   2
         Top             =   600
         Width           =   615
      End
      Begin VB.CheckBox chkTimerEnabled 
         Caption         =   "Aktiv Ved Oppstart"
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   1815
      End
      Begin VB.TextBox txtTittel 
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   6975
      End
      Begin VB.Label Label1 
         Caption         =   "Timer intervall (sekunder):"
         Height          =   375
         Left            =   4560
         TabIndex        =   22
         Top             =   600
         Width           =   1815
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Editor"
      Height          =   735
      Left            =   120
      TabIndex        =   20
      Top             =   3000
      Width           =   7455
      Begin VB.CommandButton cmdEditor 
         Caption         =   "..."
         Height          =   375
         Left            =   6480
         TabIndex        =   8
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtEditor 
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   6255
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Rotmappe"
      Height          =   735
      Left            =   120
      TabIndex        =   17
      Top             =   1320
      Width           =   7455
      Begin VB.CommandButton cmdRotmappe 
         Caption         =   "..."
         Height          =   375
         Left            =   6480
         TabIndex        =   4
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtRootPath 
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   6255
      End
   End
   Begin VB.CommandButton cmdOKstrStatus 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   4680
      TabIndex        =   13
      Top             =   6240
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   6240
      TabIndex        =   14
      Top             =   6240
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      Caption         =   "Loggmappe"
      Height          =   735
      Left            =   120
      TabIndex        =   16
      Top             =   2160
      Width           =   7455
      Begin VB.TextBox txtLogpath 
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   6255
      End
      Begin VB.CommandButton cmdLoggmappe 
         Caption         =   "..."
         Height          =   375
         Left            =   6480
         TabIndex        =   6
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Lydfilmappe"
      Height          =   2055
      Left            =   120
      TabIndex        =   15
      Top             =   3960
      Width           =   7455
      Begin VB.TextBox txtSoundXMessage 
         Height          =   285
         Left            =   120
         TabIndex        =   12
         Top             =   1560
         Width           =   6255
      End
      Begin VB.TextBox txtSoundXpath 
         Height          =   285
         Left            =   120
         TabIndex        =   11
         Top             =   960
         Width           =   6255
      End
      Begin VB.TextBox txtLydPath 
         Height          =   285
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   6255
      End
      Begin VB.CommandButton cmdLydfilmappe 
         Caption         =   "..."
         Height          =   375
         Left            =   6480
         TabIndex        =   10
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Lyd Tekst XPath (Nyhetstekstnode)"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1320
         Width           =   2775
      End
      Begin VB.Label Label2 
         Caption         =   "Lyd XPath (lydfilnode)"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   720
         Width           =   1815
      End
   End
End
Attribute VB_Name = "FormInstillinger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim recGeneralTemp As typGeneral

Private Sub cmdEditor_Click()
    recGeneral.strEditor = Me.txtEditor
    StartEdit "", " "
End Sub

Private Sub Form_Load()
    recGeneralTemp = recGeneral
    With recGeneral
        Me.txtTittel = .strTitle
        Me.chkTimerEnabled.Value = Int(.bTimerEnabledStart) * -1
        Me.txtRootPath = .strRootPath
        Me.txtInterval = .intInterval / 1000
        Me.txtLogpath = .strLoggPath
        Me.txtEditor = .strEditor
        Me.txtLydPath = .strSoundPath
        Me.txtSoundXpath = .strSoundXPath
        Me.txtSoundXMessage = .strSoundXMessage
    End With
End Sub

Private Sub HentFormFolder(objFelt As TextBox)
    If objFelt.Text = "" Then
        FormFolder.strInitPath = recGeneral.strRootPath
    Else
        FormFolder.strInitPath = objFelt.Text
    End If
    FormFolder.Show vbModal
    
    If FormFolder.strPath <> "" Then
        objFelt.Text = FormFolder.strPath
    End If
End Sub

Private Sub cmdLoggmappe_Click()
    HentFormFolder Me.txtLogpath
End Sub

Private Sub cmdLydfilmappe_Click()
    HentFormFolder Me.txtLydPath
End Sub

Private Sub cmdOKstrStatus_Click()
    On Error GoTo Err_Handler
    With recGeneral
         .strTitle = Me.txtTittel
        .bTimerEnabledStart = Me.chkTimerEnabled.Value = 1
        .strRootPath = Me.txtRootPath
        .intInterval = Me.txtInterval * 1000
        .strLoggPath = Me.txtLogpath
        .strEditor = Me.txtEditor
        .strSoundPath = Me.txtLydPath
        .strSoundXPath = Me.txtSoundXpath
        .strSoundXMessage = Me.txtSoundXMessage
    End With
    
    If recGeneral.strTitle <> recGeneralTemp.strTitle Then
        Form1.Caption = recGeneral.strTitle & " " & Form1.Tag
    End If
    
    Unload Me
    Exit Sub
    
Err_Handler:
    MsgBox Err.Description
End Sub

Private Sub cmdRotmappe_Click()
    HentFormFolder Me.txtRootPath
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub

