Attribute VB_Name = "ModRtf2Xml"
Option Explicit

'*** Konverterer RTF til XML
Public Function XMLRtfTransform(GlobLokal As typGlob, strFilnavn As String, bSkipFile As Boolean) As Boolean
'Public Function StripXml(strXMLFil As String, bSkipFile As Boolean) As String

    Dim strA As String
    Dim strHead As String
    Dim strBody As String
'    Dim strLine As String
'    Dim intFileNr As Integer

'    Set fs = New Scripting.FileSystemObject
'    Set a = fs.OpenTextFile(strXMLFil, ForReading)
'    strA = a.ReadAll

'    intFileNr = FreeFile
'    Open strXMLFil For Input As intFileNr
'    strA = ""
'    Do Until EOF(intFileNr)
'        Line Input #intFileNr, strLine
'        strA = strA & strLine & vbCrLf
'    Loop
'    Close intFileNr

    'SkrivLog "Lest fil:", strXMLFil, "StripXml"
    
    With GlobLokal
        HentXmlElements .strText, strFilnavn, bSkipFile
        If bSkipFile Then
            '*** Stoffgruppe = Priv-til-red eller Prioritet = 1
            Exit Function
        End If
    
        '*** Filas Pathname strippes n� i XCHG:
        '****.strText = Replace(.strText, "c:\SMS-Meldinger\", "")
        '*****************************
        
        .strText = ByttDato(.strText)
        .strText = Rtf2Xml(.strText)
    
        .strText = SettInnForan(.strText, "<head>" & vbCrLf, "<adm>")
        .strText = SettInnForan(.strText, "</head>" & vbCrLf, "<xmltext>")
        'a.Close
    
        If .strText = "" Then
            XMLRtfTransform = False
        Else
            XMLRtfTransform = True
        End If
        
    End With

End Function


'*** Konverterer RTF til XML
Public Function Rtf2Xml(strBody As String) As String
    Dim arrStiler(1, MAX_STILER) As String
    Dim strRtf As String
    Dim strXml As String
    Dim strStartDel As String
    Dim strSluttDel As String
    Dim strTemp As String
    Dim lngStart, lngSlutt As Long
    tmpDebug = Len(strBody)

    lngStart = InStr(1, strBody, "<rtftext><![CDATA[") + 18
    If lngStart = 18 Then
        '*** RTF-seksjonen ikke funnet, avslutter rutine.
        Rtf2Xml = strBody
        Exit Function
    End If
    
    lngSlutt = InStr(lngStart, strBody, "]]></rtftext>")
    
    If lngSlutt = 0 Then
        Rtf2Xml = ""
        Exit Function
    End If
    
    strRtf = Mid(strBody, lngStart, lngSlutt - lngStart)
    strStartDel = Mid(strBody, 1, lngStart - 19)
    strSluttDel = Mid(strBody, lngSlutt + 13)
    
    FyllStiler arrStiler, strRtf
    strXml = LagXmlAvRtf(arrStiler, strRtf)
    
    'Rtf2Xml = strStartDel & Replace(strXml, "&", "&amp;") & strSluttDel
    Rtf2Xml = strStartDel & strXml & strSluttDel
End Function

Function LagXmlAvRtf(arrStiler() As String, strRtf As String)
    Dim strRtfBody As String
    Dim strStilNavn As String
    Dim strRtfPara As String
    Dim strXml As String
    Dim intStilNr As Integer
    Dim lngStart As Long
    Dim lngSlutt As Long
    Dim lngStartPara As Long
    Dim lngSluttPara As Long
    Dim lngStartTabell As Long
    Dim Slutt As Boolean
    Dim NyPar As Boolean
    Dim strTabellXML As String
    Dim strTabellRtf As String
    
    Dim temp
    
    lngStart = InStr(1, strRtf, "\pard")
    If lngStart = 0 Then
        '*** RTF-Stil seksjonen ikke funnet, avslutter rutine.
        Exit Function
    End If
    lngSlutt = InStr(lngStart, strRtf, "}}") + 2
    strRtfBody = Mid(strRtf, lngStart, lngSlutt - lngStart)
    
    lngStart = 1
    NyPar = True
    Do
        If NyPar Then
            'lngStart = InStr(lngStart + 10, strRtfBody, "\s")
            lngStart = InStr(lngStart, strRtfBody, "\s")
            If lngStart = 0 Then
                Exit Do
            End If
            lngSlutt = InStr(lngStart + 2, strRtfBody, "\")
            intStilNr = Val(Mid(strRtfBody, lngStart + 2, lngSlutt - lngStart))
            
            If intStilNr < MAX_STILER Then
                strStilNavn = arrStiler(0, intStilNr)
                If Trim(strStilNavn) = "" Then
                    strStilNavn = "empty"
                End If
            Else
                strStilNavn = "_brodtekst"
            End If
            
            lngStartPara = lngSlutt
            strXml = strXml & "<" & strStilNavn & ">" & vbCrLf
        End If
    
        lngSluttPara = InStr(lngStartPara + 1, strRtfBody, "\par ")
        If lngSluttPara = 0 Then
            strXml = strXml & "</" & strStilNavn & ">" & vbCrLf
            Exit Do
        End If
            
        
        strRtfPara = Mid(strRtfBody, lngStartPara, lngSluttPara - lngStartPara)
        'strRtfPara = "<![CDATA[" & RensKoder(strRtfPara) & "]]>"
        strRtfPara = RensKoder(strRtfPara)
        
        strXml = strXml & "<p>" & strRtfPara & "</p>" & vbCrLf
        
       
        '*** Finner Tabell eller slutt p� avsnitt
        lngStartTabell = InStr(lngSluttPara + 4, Left(strRtfBody, lngSluttPara + 20), "\trowd")
        lngStart = InStr(lngSluttPara + 4, Left(strRtfBody, lngSluttPara + 10), "\pard")
        
        If lngStartTabell <> 0 Then
            lngStart = lngStartTabell
            lngSlutt = InStr(lngStart, strRtfBody, "\row }\pard")
            strTabellRtf = Mid(strRtfBody, lngStart, lngSlutt - lngStart)

            strTabellXML = HentTabell(strTabellRtf)
            strXml = strXml & strTabellXML
            
            lngStart = lngSlutt
        End If
        
        If lngStart <> 0 Then
            NyPar = True
            lngStartPara = lngStart
            strXml = strXml & "</" & strStilNavn & ">" & vbCrLf
        Else
            NyPar = False
            lngStart = lngSluttPara + 4
            lngStartPara = lngSluttPara + 4
        End If
        
        DoEvents
    Loop 'Until Slutt
            
    LagXmlAvRtf = "<xmltext>" & vbCrLf & strXml & vbCrLf & "</xmltext>"
   
End Function

Function RensKoder(strRtf As String) As String
    Dim strTemp As String
    Dim lngStart As Long
    Dim lngSlutt As Long
    
    strTemp = strRtf & " "
    
    'strTemp = Replace(strTemp, Chr(148), """")
    'strTemp = Replace(strTemp, "\rquote ", "'")
    strTemp = Replace(strTemp, "\rquote ", "&quot;")
    strTemp = Replace(strTemp, "\endash ", "-")
    strTemp = Replace(strTemp, "{\*\bkmkstart CursorPos}", "")
    strTemp = Replace(strTemp, "{\*\bkmkend CursorPos}", "")
    '{\*\bkmkstart CursorPos}
    '{\*\bkmkend CursorPos}
    'strTemp = Replace(strTemp, "\cell ", "<td/>")
    'strTemp = Replace(strTemp, "{", "")
    
    lngStart = 1
    Do
        lngStart = InStr(lngStart, strTemp, "\")
        If lngStart = 0 Then Exit Do
        lngSlutt = InStr(lngStart, strTemp, " ")
        strTemp = Mid(strTemp, 1, lngStart - 1) & Mid(strTemp, lngSlutt + 1)
        lngStart = 1
    Loop
    
    strTemp = Replace(strTemp, "{", "")
    strTemp = Replace(strTemp, "}", "")
    strTemp = Replace(strTemp, vbCrLf, " ")
    strTemp = Replace(strTemp, "  ", " ")
    strTemp = Trim(strTemp)
    RensKoder = strTemp
End Function

Sub FyllStiler(arrStiler() As String, strRtf As String)
    Dim strStiler As String
    Dim strStilDef As String
    Dim strStilNavn As String
    Dim intStilNr As Integer
    Dim lngStart As Long
    Dim lngSlutt As Long

    lngStart = InStr(1, strRtf, "{\stylesheet")
    If lngStart = 0 Then
        '*** RTF-Stil seksjonen ikke funnet, avslutter rutine.
        Exit Sub
    End If
    lngSlutt = InStr(lngStart, strRtf, "}}") + 2
    strStiler = Mid(strRtf, lngStart, lngSlutt - lngStart)
    
    
    lngStart = InStr(10, strStiler, "{\s")
    Do Until lngStart = 0
        lngSlutt = InStr(lngStart, strStiler, ";}") + 2
        strStilDef = Mid(strStiler, lngStart, lngSlutt - lngStart)
        
        HentStilNrNavn strStilDef, intStilNr, strStilNavn
        
        If intStilNr < MAX_STILER Then
            strStilNavn = Replace(strStilNavn, "_", "")
            strStilNavn = Replace(strStilNavn, " ", "")
            strStilNavn = Replace(strStilNavn, "/", "")
            strStilNavn = Replace(strStilNavn, "\", "")
            strStilNavn = Replace(strStilNavn, "(", "")
            strStilNavn = Replace(strStilNavn, ")", "")
            strStilNavn = Replace(strStilNavn, ":", ".")
            strStilNavn = Replace(strStilNavn, "�", "o", 1, -1, vbTextCompare)
    
            strStilNavn = LCase(strStilNavn)
            
            arrStiler(0, intStilNr) = strStilNavn
            arrStiler(1, intStilNr) = strStilDef
        End If
        
        lngStart = InStr(lngSlutt, strStiler, "{\s")
    Loop
   

End Sub

Sub HentStilNrNavn(strStilDef As String, intStilNr As Integer, strStilNavn As String)
    Dim lngStart As Long
    Dim lngSlutt As Long
    Dim temp
   
    intStilNr = Val(Mid(strStilDef, 4, 4))
    
    temp = Right(strStilDef, 30)
    
    lngStart = InStr(1, strStilDef, "\snext")
    If lngStart = 0 Then
        '*** RTF-Stil seksjonen ikke funnet, avslutter rutine.
        Exit Sub
    End If
    lngStart = InStr(lngStart, strStilDef, " ") + 1
    lngSlutt = InStr(lngStart, strStilDef, ";}")

    strStilNavn = Mid(strStilDef, lngStart, lngSlutt - lngStart)
    
End Sub

Function HentTabell(strTabellRtf As String) As String
    Dim strRad As String
    Dim lngSlutt As Long
    Dim lngStart As Long
    Dim strXml As String
    Dim strRadXml As String
            
    strXml = "<table>" & vbCrLf
    
    lngStart = 1
    'If lngStart = 0 Then Exit Do
    
    Do
        strXml = strXml & "<tr>" & vbCrLf
        
        ' Finn start og slutt p� tabellrader
        lngStart = InStr(lngStart, strTabellRtf, "{")
        If lngStart = 0 Then Exit Do
        
        lngSlutt = InStr(lngStart, strTabellRtf, "\trowd ")
        
        If lngSlutt = 0 Then
            Exit Do
        End If
        
        strRad = Mid(strTabellRtf, lngStart, lngSlutt - lngStart)
        '*** Debug:
        'tmpDebug = Right(strRad, 50)
        
        strRadXml = HentCeller(strRad)
        
        strXml = strXml & strRadXml
        strXml = strXml & "</tr>" & vbCrLf
        
        lngStart = lngSlutt + 7
        DoEvents
    Loop
    
    strXml = strXml & "</tr>" & vbCrLf
    strXml = strXml & vbCrLf & "</table>" & vbCrLf
    
    HentTabell = strXml

End Function

Private Function HentCeller(strRad) As String
    Dim strXml As String
    Dim strCelle As String
    Dim lngStart As Long
    Dim lngSlutt As Long
    lngStart = 2
    Do
        ' celler
        lngSlutt = InStr(lngStart, strRad, "\cell ")
        If lngSlutt = 0 Then Exit Do
        strCelle = Mid(strRad, lngStart, lngSlutt - lngStart)
        strXml = strXml & "<td>" & RensKoder(strCelle) & "</td>" & vbCrLf
        'strXML = strXML & "<td>" & strCelle & "</td>"
        lngStart = lngSlutt + 6
    Loop
    HentCeller = strXml
End Function

Private Sub HentXmlElements(strA As String, strFilnavn As String, bSkipFile As Boolean)
    On Error GoTo Err_Handler
    Dim xmlDoc As DOMDocument
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlError As IXMLDOMParseError
    Dim xmlNode As IXMLDOMNode
    Dim strPrioritet As String
    Dim strGroup As String
    Dim strSoundFile As String
    Dim i As Integer
    Dim n As Integer
    Dim strRefnum As String
    Dim strCode As String
    Dim strType As String
    Dim strElement As String
    Dim strXml  As String
    Dim intStart As Integer
    Dim intSlutt As Integer
    Dim strSubElement As String
    Dim strNewsBody As String
    Dim arrCategory As Variant
    Dim intCatNr As Integer
    Dim strText As String
       
    bSkipFile = False
    
    If Not LoadXmlDoc(xmlDoc, strA, False, strMessage, True) Then
        SkrivErr "hentXmlElemets", strMessage, 0, strFilnavn
        Exit Sub
    End If
            
    '*** Henter xml-elementer (tag-innhold) eller attributter fra entydige noder:
    strGroup = HentXmlNodeText(xmlDoc, "/message/group", "")
    'strPrioritet = HentXmlNodeText(xmlDoc, "/message/fields/field[name='NTBPrioritet']/value", "")
    If strGroup = "Priv-til-red" Then  ' Or strPrioritet = "1" Then
        bSkipFile = True
    Else
        bSkipFile = False
    End If
    
    strXml = ""
    
    strSoundFile = HentXmlNodeText(xmlDoc, recGeneral.strSoundXPath, "")
    If strSoundFile <> "" Then
        strNewsBody = HentXmlNodeText(xmlDoc, recGeneral.strSoundXMessage, "")
        LagLydXml strSoundFile, strNewsBody, strFilnavn
        strXml = strXml & LagXmlTag("soundsource", GetFileBase(strSoundFile, False))
    End If
    
    strElement = HentXmlNodeText(xmlDoc, "/message/category", "")
    arrCategory = Split(strElement, ";")
    For intCatNr = 0 To UBound(arrCategory)
        strText = arrCategory(intCatNr)
        If strText <> "" Then
            strXml = strXml & LagXmlTag("categories", strText)
        End If
    Next
        
    'strElement = HentXmlNodeText(xmlDoc, "/message/subcategory/field/value", "")
    '<subcategory>
    '<field><name>NTBUnderKatSport</name><value><![CDATA[Fotball;]]></value></field>
    '</subcategory>
        
    strA = SettInnForan(strA, strXml, "<sms>")
    
    Set xmlDoc = Nothing
    Exit Sub
Err_Handler:
    SkrivErr "HentXmlElements", Err.Description, "", strFilnavn
End Sub

Private Function SettInnForan(strDoc As String, strDel, strTag As String) As String
    Dim lngStart As Long
    
    lngStart = InStr(strDoc, strTag)
    If lngStart = 0 Then
        SettInnForan = strDoc
    Else
        SettInnForan = Left(strDoc, lngStart - 1) & strDel & Mid(strDoc, lngStart)
    End If
   
End Function

Private Sub LagLydXml(strSoundFile As String, strNewsBody As String, strFilnavn As String)
    On Error GoTo Err_Handler
    Dim strOutFile As String
    Dim intFile As Integer
    
    
        strOutFile = recGeneral.strSoundPath & "\snd_" & GetFileBase(strFilnavn) & ".xml"

        intFile = FreeFile
        Open strOutFile For Output As #intFile
        Print #intFile, "<?xml version=""1.0"" encoding=""ISO-8859-1"" standalone=""yes""?>"
        Print #intFile, "<message>"
        Print #intFile, "<text><![CDATA["
        Print #intFile, strNewsBody
        Print #intFile, "]]></text>"
        Print #intFile, "<soundfile>" & strSoundFile & "</soundfile>"
        Print #intFile, "<soundmp3>" & GetFileBase(strSoundFile, True) & "</soundmp3>"
        Print #intFile, "<soundsource>" & GetFileBase(strSoundFile, False) & "</soundsource>"
        Print #intFile, "</message>"
        
        Close #intFile
        
        SkrivLog "Lydfil: " & "Lyd", strOutFile

    Exit Sub
Err_Handler:
    SkrivErr "LagLydfil", Err.Description, "", strOutFile
End Sub
