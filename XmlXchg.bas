Attribute VB_Name = "XmlXchg"
Option Explicit

Public Const STR_CRLF = "<CrLf/>"

Public arrXmlXchg() As typXmlXchg
Public arrXchgReplace() As typXchgReplace

Type typXmlXchg
    intJobNr As Integer
    strType As String
    strXpath As String
    bCData As Boolean
    lpReplaceStart As Long
    lpReplaceEnd As Long
    lpNexNode As Long
    lpPrevNode As Long
End Type

Type typXchgReplace
    strFind As String
    strReplace As String
    strFind2 As String
    strReplace2 As String
    
    strFindShow As String
    strReplaceShow As String
    strFind2Show As String
    strReplace2Show As String
    
    bFindEnt As Boolean
    bReplaceEnt As Boolean
    bFind2Ent As Boolean
    bReplace2Ent As Boolean
    
    intStart As Long
    intCount As Integer
    intCompare As Integer
    lpNexNode As Long
    lpPrevNode As Long
End Type

Public Function InitXmlXchg(intJobNr As Integer) As Boolean
    Dim strFilnavn As String
    Dim xmlDoc As DOMDocument
    
    With arrTrans(intJobNr)
        If LCase(Right(.strXchgFile, 4) <> ".xml") Then
            InitXmlXchg = True
            .lpXchgStart = 0
            Exit Function
        End If
        
        strFilnavn = recGeneral.strRootPath & "\xchg\" & .strXchgFile
        If Not LoadXmlDoc(xmlDoc, strFilnavn, True, strMessage, True) Then
            .lpXchgStart = 0
            SkrivErr "InitXmlXchg", strMessage, intJobNr, strFilnavn
            InitXmlXchg = False
            Exit Function
        End If
        
        FyllXmlXchg xmlDoc, "/xchg/*", intJobNr, .lpXchgStart, .lpXchgEnd
        
    End With
    
    InitXmlXchg = True
Err_Handler:
    strMessage = Err.Description
End Function

Private Sub FyllXmlXchg(xmlDoc As DOMDocument, strXpath As String, intJobNr As Integer, lpXchgStart As Long, lpXchgEnd As Long)
    Dim lpIndex As Long
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlNode As IXMLDOMNode
    
    lpIndex = UBound(arrXmlXchg)
    lpXchgStart = lpIndex + 1
    Dim i As Integer
    Set xmlNodeList = xmlDoc.selectNodes(strXpath)
    For i = 0 To xmlNodeList.length - 1
        Set xmlNode = xmlNodeList(i)
        lpIndex = lpIndex + 1
        ReDim Preserve arrXmlXchg(lpIndex)
        With arrXmlXchg(lpIndex)
            .intJobNr = intJobNr
            .strType = xmlNode.nodeName
            Select Case .strType
            Case "xpath"
                arrTrans(intJobNr).bXmlXchg = True
            Case "last"
                arrTrans(intJobNr).bLastXchg = True
            Case Else
                arrTrans(intJobNr).bXchg = True
            End Select
            
            .strXpath = xmlNode.Attributes.Item(0).Text
            FyllXchgReplace xmlNode.childNodes, intJobNr, .lpReplaceStart, .lpReplaceEnd, .strType
            .lpNexNode = lpIndex + 1
            If i = 0 Then
                .lpPrevNode = 0
            Else
                .lpPrevNode = lpIndex - 1
            End If
        End With
    Next
    arrXmlXchg(lpIndex).lpNexNode = 0
    lpXchgEnd = lpIndex

End Sub

Private Sub FyllXchgReplace(xmlNodeList As IXMLDOMNodeList, intJobNr As Integer, lpStart As Long, lpEnd As Long, strType As String)
    Dim lpIndex As Long
    Dim i As Integer
    Dim xmlNode As IXMLDOMNode
    
    lpIndex = UBound(arrXchgReplace)
    lpStart = lpIndex + 1
    For i = 0 To xmlNodeList.length - 1
        Set xmlNode = xmlNodeList(i)
        lpIndex = lpIndex + 1
        ReDim Preserve arrXchgReplace(lpIndex)
        With arrXchgReplace(lpIndex)
            .intStart = xmlNode.Attributes.Item(0).Text
            .intCount = xmlNode.Attributes.Item(1).Text
            .intCompare = xmlNode.Attributes.Item(2).Text
            
            .strFindShow = xmlNode.childNodes(0).Text
            .strFindShow = Replace(.strFindShow, STR_CRLF, vbCrLf)
            .bFindEnt = xmlNode.childNodes(0).Attributes.Item(0).Text = "True"
            If .bFindEnt Then
                .strFind = .strFindShow
            Else
                .strFind = LagCharStreng(.strFindShow)
            End If
            
            .strReplaceShow = xmlNode.childNodes(1).Text
            .strReplaceShow = Replace(.strReplaceShow, STR_CRLF, vbCrLf)
            .bReplaceEnt = xmlNode.childNodes(1).Attributes.Item(0).Text = "True"
            If .bReplaceEnt Then
                .strReplace = .strReplaceShow
            Else
                .strReplace = LagCharStreng(.strReplaceShow)
            End If
         
            If strType = "bulk" Then
                .strFind2Show = LagCharStreng(xmlNode.childNodes(2).Text)
                .strFind2Show = Replace(.strFind2Show, STR_CRLF, vbCrLf)
                .bFind2Ent = xmlNode.childNodes(2).Attributes.Item(0).Text = "True"
                If .bFindEnt Then
                    .strFind2 = .strFind2Show
                Else
                    .strFind2 = LagCharStreng(.strFind2Show)
                End If

            ElseIf strType = "pair" Then
                .strFind2Show = LagCharStreng(xmlNode.childNodes(2).Text)
                .strFind2Show = Replace(.strFind2Show, STR_CRLF, vbCrLf)
                .bFind2Ent = xmlNode.childNodes(2).Attributes.Item(0).Text = "True"
                If .bFindEnt Then
                    .strFind2 = .strFind2Show
                Else
                    .strFind2 = LagCharStreng(.strFind2Show)
                End If
                
                .strReplace2Show = LagCharStreng(xmlNode.childNodes(3).Text)
                .strReplace2Show = Replace(.strReplace2Show, STR_CRLF, vbCrLf)
                .bReplace2Ent = xmlNode.childNodes(3).Attributes.Item(0).Text = "True"
                If .bReplaceEnt Then
                    .strReplace2 = .strReplace2Show
                Else
                    .strReplace2 = LagCharStreng(.strReplace2Show)
                End If
            End If
            
            .lpNexNode = lpIndex + 1
            If i = 0 Then
                .lpPrevNode = 0
            Else
                .lpPrevNode = lpIndex - 1
            End If
        End With
    Next
    arrXchgReplace(lpIndex).lpNexNode = 0
    lpEnd = lpIndex
End Sub

Public Function DoTxtXchg(strDoc As String, intJobNr As Integer, Optional strLast As String) As Boolean
    On Error GoTo Err_Handler
    Dim lpIndex As Long
    Dim lpPrevIndex As Long
    Dim bSkipDebug As Boolean
    
    With arrTrans(intJobNr)
        If .strXchgFile = "" Then Exit Function
        lpIndex = .lpXchgStart
        
        If .bDebug Then
            VisDebug intJobNr, 1, strDoc, "", lpIndex
        End If
    End With
        
    Do
        With arrXmlXchg(lpIndex)
            bSkipDebug = False
            Select Case .strType & strLast
            Case "lastlast"
                '*** Kun hvis ogs� strLast = "last"
                doTxtReplace strDoc, .lpReplaceStart
            Case "plain"
                doTxtReplace strDoc, .lpReplaceStart
            Case "bulk"
                doBulkReplace strDoc, .lpReplaceStart
            Case "pair"
                doPairReplace strDoc, .lpReplaceStart
            Case Else
                bSkipDebug = True
            End Select
            lpPrevIndex = lpIndex
            lpIndex = .lpNexNode
        End With
        If arrTrans(intJobNr).bDebug And Not bSkipDebug Then
            VisDebug intJobNr, 2, "", strDoc, lpPrevIndex
        End If
    Loop Until lpIndex = 0
        
    
    DoTxtXchg = True
    Exit Function
Err_Handler:
    DoTxtXchg = False
End Function

Public Function doBulkReplace(strDoc As String, lpReplaceStart As Long) As Boolean
    
    Dim lpIndex As Long
    lpIndex = lpReplaceStart
    Do
        With arrXchgReplace(lpIndex)
            strDoc = BulkReplace(strDoc, .strFind, .strFind2, .strReplace, .intStart, .intCount)
            lpIndex = .lpNexNode
        End With
    Loop Until lpIndex = 0

End Function

Public Function BulkReplace(strDoc As String, strFind As String, strFind2 As String, strReplace As String, intStart As Long, intCount As Integer) As String
    Dim i As Long
    Dim lpStart As Long
    Dim lpSlutt As Long
    Dim intToCount As Long
    
    If strFind = "" Or strFind2 = "" Then Exit Function

    If intCount < 0 Then
        intToCount = 10000
    Else
        intToCount = intCount
    End If
    
    lpStart = intStart
    BulkReplace = strDoc
    For i = 1 To intToCount
        lpStart = InStr(lpStart, BulkReplace, strFind)
        If lpStart = 0 Then Exit For
        lpSlutt = InStr(lpStart + Len(strFind), BulkReplace, strFind2)
        If lpSlutt = 0 Then Exit For
        BulkReplace = Mid(BulkReplace, 1, lpStart - 1) & strReplace & Mid(BulkReplace, lpSlutt + Len(strFind2))
    Next
    
End Function

Public Function doPairReplace(strDoc As String, lpReplaceStart As Long) As Boolean
    
    Dim lpIndex As Long
    lpIndex = lpReplaceStart
    Do
        With arrXchgReplace(lpIndex)
            strDoc = PairReplace(strDoc, .strFind, .strFind2, .strReplace, .strReplace2, .intStart, .intCount)
            lpIndex = .lpNexNode
        End With
    Loop Until lpIndex = 0

End Function

Public Function PairReplace(strDoc As String, strFind As String, strFind2 As String, strReplace As String, strReplace2 As String, intStart As Long, intCount As Integer) As String
    Dim i As Long
    Dim lpStart As Long
    Dim lpSlutt As Long
    Dim intToCount As Long
    
    If strFind = "" Or strFind2 = "" Then Exit Function
    
    If intCount < 0 Then
        intToCount = 100000
    Else
        intToCount = intCount
    End If
    
    lpStart = intStart
    PairReplace = strDoc
    For i = 1 To intToCount
        lpStart = InStr(lpStart, PairReplace, strFind)
        If lpStart = 0 Then Exit For
        lpSlutt = InStr(lpStart + Len(strFind), PairReplace, strFind2)
        If lpSlutt = 0 Then Exit For
        PairReplace = Mid(PairReplace, 1, lpStart - 1) & strReplace & Mid(PairReplace, lpStart + Len(strFind), lpSlutt - lpStart - Len(strFind)) & strReplace2 & Mid(PairReplace, lpSlutt + Len(strFind2))
        lpStart = lpSlutt
    Next
    
End Function

Public Function doTxtReplace(strDoc As String, lpReplaceStart As Long) As Boolean
    
    Dim lpIndex As Long
    lpIndex = lpReplaceStart
    Do
        With arrXchgReplace(lpIndex)
            strDoc = Replace(strDoc, .strFind, .strReplace, .intStart, .intCount, .intCompare)
            lpIndex = .lpNexNode
        End With
    Loop Until lpIndex = 0
    
End Function

Public Function DoXmlXchg(xmlDoc As DOMDocument, intJobNr As Integer) As Boolean
    On Error GoTo Err_Handler
    Dim xmlNodeList As IXMLDOMNodeList
    Dim i As Integer
    Dim strXpath As String
    Dim strXml As String
    Dim xmlXpathNode As IXMLDOMNode
    Dim lpIndex As Long
    
    With arrTrans(intJobNr)
        If .strXchgFile = "" Then Exit Function
        lpIndex = .lpXchgStart
    End With
    Do
        With arrXmlXchg(lpIndex)
            Select Case .strType
            'Case "first", "last"
            'Case "bulk"
            '    doBulkReplace
            'Case "pair"
                'doPairReplace
            Case "xpath"
                doXpathReplace xmlDoc, .strXpath, .lpReplaceStart, .bCData
            End Select
            lpIndex = .lpNexNode
        End With
    Loop Until lpIndex = 0
    
    DoXmlXchg = True
    Exit Function
Err_Handler:
    DoXmlXchg = False
End Function

Public Function doXpathReplace(xmlDoc As DOMDocument, strXpath As String, lpReplaceStart As Long, Optional bCData As Boolean) As Boolean
    Dim strText As String
    Dim i As Long
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlNodeList2 As IXMLDOMNodeList
    
    Set xmlNodeList = xmlDoc.selectNodes(strXpath)
    
    For i = 0 To xmlNodeList.length - 1
        Set xmlNodeList2 = xmlNodeList.Item(i).childNodes
        Select Case xmlNodeList.Item(i).nodeType
        Case 2
            xmlNodeList.Item(i).Text = xmlReplace(xmlNodeList.Item(i).Text, lpReplaceStart)
        Case 3, 4
            xmlNodeList.Item(i).Text = xmlReplace(xmlNodeList.Item(i).Text, lpReplaceStart)
        Case 1
            'If intLevel < 1 Then
            '    xmlSearchTree xmlNodeList.Item(i).childNodes, xmlXpathNode, bCData, 1
            'End If
            
            Set xmlNodeList2 = xmlNodeList.Item(i).childNodes
            'strText = xmlNodeList2(0).nodeName
            'strText = xmlNodeList2(0).Text
            Select Case xmlNodeList2.Item(0).nodeType
            Case 2
                xmlNodeList2.Item(0).Text = xmlReplace(xmlNodeList2.Item(0).Text, lpReplaceStart)
            Case 3, bCData * -4
                xmlNodeList2.Item(0).Text = xmlReplace(xmlNodeList2.Item(0).Text, lpReplaceStart)
            Case 1
            End Select
            
        End Select
    Next
    
End Function
   
Public Function xmlReplace(strXmlKilde As String, lpReplaceStart As Long) As String
    Dim lpIndex As Long
    xmlReplace = strXmlKilde
    lpIndex = lpReplaceStart
    Do
        With arrXchgReplace(lpIndex)
            xmlReplace = Replace(xmlReplace, .strFind, .strReplace, .intStart, .intCount, .intCompare)
            lpIndex = .lpNexNode
        End With
    Loop Until lpIndex = 0

End Function

