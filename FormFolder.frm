VERSION 5.00
Begin VB.Form FormFolder 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Velg mappe"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5655
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   5655
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   4560
      Width           =   5415
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   4320
      TabIndex        =   3
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3120
      TabIndex        =   2
      Top             =   4920
      Width           =   1095
   End
   Begin VB.DirListBox Dir1 
      Height          =   3915
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   5415
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5415
   End
End
Attribute VB_Name = "FormFolder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strDrive As String
Public strPath As String
Public strInitPath As String

Private Sub cmdAvbryt_Click()
    strPath = ""
    Unload Me
End Sub

Private Sub cmdOK_Click()
    'strFormName
    Unload Me
End Sub

Private Sub Form_Load()
    On Error GoTo err_handler
    Me.Drive1.Drive = strInitPath
    Me.Dir1.Path = strInitPath
    strDrive = strInitPath
Exit Sub
err_handler:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub Dir1_Change()
    On Error GoTo err_handler
    Me.Text1 = Me.Dir1.Path
    strPath = Me.Dir1.Path
Exit Sub
err_handler:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub Drive1_Change()
    On Error GoTo err_handler
    Me.Text1 = Me.Drive1.Drive
    Me.Dir1.Path = Me.Drive1.Drive
    strDrive = Me.Drive1.Drive
Exit Sub
err_handler:
    MsgBox Err.Description, vbCritical
    Me.Drive1.Drive = strDrive
End Sub

