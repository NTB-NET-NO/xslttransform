Attribute VB_Name = "FuncModule"
'*** Funksjoner og Prosedyrer
Option Explicit

Public Sub CopyError(intJobNr As Integer, strInFile As String)
    Dim strErrPath As String
    'Dim strFleName As String
    
    strErrPath = recGeneral.strRootPath & "\" & "ERROR"
    MakePath strErrPath
    
    strErrPath = strErrPath & "\job" & arrTrans(intJobNr).strNr & "\"
    MakePath strErrPath
    
    FSO.CopyFile strInFile, strErrPath
        
End Sub

Public Function doReplace(strText As String, intXchgLines As Integer) As String
    Dim strTemp
    Dim n As Integer
    
    strTemp = strText
    For n = 1 To intXchgLines
        With arrXchg(n)
            strTemp = Replace(strTemp, .strFrom, .strTo)
        End With
    Next
    doReplace = strTemp
End Function

Public Function FileArchive(strArchive As String, strFile As String, Optional f As File) As Boolean
    FileArchive = False
    Set f = FSO.GetFile(strFile)
    Select Case LCase(strArchive)
    Case STR_MODIFY
        If f.Attributes = Archive Then
            FileArchive = True
            f.Attributes = Normal
        End If
    Case STR_NO_MOD
        If f.Attributes = Archive Then
            FileArchive = True
        End If
    Case Else
        FileArchive = True
    End Select

End Function

Public Sub DeleteFile(intJobNr As Integer, strFile As String)
    On Error GoTo Err_Handler
    Dim strMessage As String
    Dim bOK As Boolean
    Dim f As File
    Dim intDay As Integer
    
    With arrTrans(intJobNr)
        
        If .strDelete = STR_YES Then
            Set f = FSO.GetFile(strFile)
            f.Delete False
            SkrivLog "JobNr:" & Format(Str(intJobNr), "00") & " " & .strType, strFile, "Slettet "
        ElseIf .strDelete = STR_DAYS Then
            Set f = FSO.GetFile(strFile)
            intDay = .strDelDays
            If DateDiff("d", f.DateLastModified, Now()) > intDay Then
                f.Delete False
                SkrivLog "JobNr:" & Format(Str(intJobNr), "00") & " " & .strType, strFile, "Slettet "
            End If
        End If
   End With
   Exit Sub
    
Err_Handler:
   SkrivErr "DeleteFile", Err.Description, intJobNr, strFile
        
End Sub

Public Function FyllTransRec(trRec As typTransRec, intIndex As Integer) As Boolean
    Dim strAppl As String
    Dim strNr As String
    Dim strTemp As String
    
    strNr = Format(Str(intIndex), "00")
    strAppl = "Transform" & strNr
    With trRec
        .strNr = strNr
        .strType = HentIniStreng(Ini_File, strAppl, "Type", "")
        If .strType = "" Then
            FyllTransRec = False
            Exit Function
        End If
        .strText = HentIniStreng(Ini_File, strAppl, "Text", "")
        .strXsltFile = HentIniStreng(Ini_File, strAppl, "TransFile", "")
        .strXchgFile = HentIniStreng(Ini_File, strAppl, "XchgFile", "")
        .strCopyFile = HentIniStreng(Ini_File, strAppl, "CopyFile", "")
        .strInPath = HentIniStreng(Ini_File, strAppl, "InPath", "")
        .strOutPath = HentIniStreng(Ini_File, strAppl, "OutPath", "")
        .strFileExt = HentIniStreng(Ini_File, strAppl, "FileExt", "")
        .strLogFile = recGeneral.strRootPath & "\" & LOG_PATH & "\" & HentIniStreng(Ini_File, strAppl, "LogFile", "")
        .strDelete = HentIniStreng(Ini_File, strAppl, "Delete", "")
        .strDelDays = HentIniStreng(Ini_File, strAppl, "DelDays", "")
        .strArchive = HentIniStreng(Ini_File, strAppl, "Archive", "")
        .strStatus = HentIniStreng(Ini_File, strAppl, "Status", "")

    End With
    FyllTransRec = True
End Function

Public Function InitXslt(strXslFile As String, xmlDoc As DOMDocument, xsltProc As IXSLProcessor, strMessage As String, Optional intJobNr As Integer) As Boolean
    On Error GoTo Err_Handler
    Dim xslt As XSLTemplate
    Dim xsltDoc As FreeThreadedDOMDocument
    Dim xmlError As IXMLDOMParseError
    
    Set xslt = New XSLTemplate
    Set xsltDoc = New FreeThreadedDOMDocument
    
    xsltDoc.Load strXslFile
    Set xmlError = xsltDoc.parseError

    If (xmlError.errorCode <> 0) Then
        SkrivErr "InitXslt", xmlError.reason & " i linje: " & xmlError.Line, intJobNr, strXslFile
        InitXslt = False
        Exit Function
    End If

    Set xslt.stylesheet = xsltDoc
       
    Set xmlDoc = New DOMDocument
    xmlDoc.async = False
        
    Set xsltProc = xslt.createProcessor
    xsltProc.input = xmlDoc
    
    Set xslt = Nothing
    Set xsltDoc = Nothing
    
    InitXslt = True
Exit Function

Err_Handler:
    InitXslt = False
    SkrivErr "InitXslt", Err.Description, intJobNr, strXslFile
    
End Function

Public Function InitXchg(strXchgFile As String, intXchgLines As Integer, strMessage As String) As Boolean
    Dim a As TextStream
    Dim intAntLine As Integer
    Dim intLen As Integer
    Dim intPos As Integer
    Dim i As Integer
    Dim strLine As String
    Dim strFra As String
    Dim strTil As String
    Dim strStart As String
    Dim strCount As String
    Dim strOption As String
    
    InitXchg = True
    i = 1
    
    Set a = FSO.OpenTextFile(strXchgFile, ForReading)
    Do Until a.AtEndOfStream
        strLine = a.ReadLine
        strLine = Replace(strLine, Chr(9), ", ")
        intLen = Len(strLine)
        
        intPos = InStr(1, strLine, ">>, ")
        If intPos > 0 Then
            strFra = Left(strLine, intPos + 2)
            strTil = Right(strLine, intLen - (intPos + 3))
        Else
            intPos = InStr(1, strLine, ", ")
            If intPos > 0 Then
                strFra = Left(strLine, intPos - 1)
                strTil = Right(strLine, intLen - (intPos + 1))
            End If
        End If
        
        arrXchg(i).strFrom = LagCharStreng(strFra)
        arrXchg(i).strTo = LagCharStreng(strTil)
        arrXchg(i).lngStart = 1
        arrXchg(i).lngCount = -1
        arrXchg(i).intOption = 0
        
        i = i + 1
    Loop
    a.Close
    Set a = Nothing
    intXchgLines = i - 1
End Function

Public Function LagCharStreng(strStreng) As String
    Dim lngStart As Long
    Dim lngSlutt As Long
    Dim strChar As String
    Dim intChar As String
    Dim strTemp As String
    
    strTemp = strStreng
    
    lngStart = 1
    lngStart = InStr(lngStart, strTemp, "&")
    
    Do Until lngStart = 0
        lngSlutt = InStr(lngStart, strTemp, ";")
        If lngSlutt = 0 Then Exit Do
        strChar = Mid(strTemp, lngStart + 1, lngSlutt - lngStart - 1)
        If Left(strChar, 1) = "#" Then
            If strChar = "#" Then
                strChar = ""
            Else
                intChar = Val(Mid(strChar, 2))
                strChar = Chr(intChar)
            End If
            strTemp = Mid(strTemp, 1, lngStart - 1) & strChar & Mid(strTemp, lngSlutt + 1)
        End If
        lngStart = InStr(lngStart + 1, strTemp, "&")
        
    Loop
    LagCharStreng = strTemp
End Function


Public Sub CheckPath(intJobNr As Integer)
    On Error GoTo Err_Handler
        
    With arrTrans(intJobNr)
        PathExist "", .strInPath, True, False, "CheckPath2", intJobNr
        PathExist "", .strOutPath, True, True, "CheckPath2", intJobNr
        
        PathExist recGeneral.strRootPath & "\xslt\", .strXsltFile, False, False, "CheckPath", intJobNr
        PathExist recGeneral.strRootPath & "\copy\", .strCopyFile, False, False, "CheckPath", intJobNr
        PathExist recGeneral.strRootPath & "\xchg\", .strXchgFile, False, False, "CheckPath", intJobNr
    End With
    
Exit Sub
    
Err_Handler:
    SkrivErr "CheckPath", strMessage, intJobNr
End Sub

Public Sub PathExist(strRootPath As String, strPath As String, bFolder As Boolean, bMakePath As Boolean, strFuncName As String, intJobNr As Integer)
    On Error GoTo Err_Handler
    Dim bExist
    
    If strPath = "" Then Exit Sub
    
    bExist = True
    If bFolder Then
        If Not FSO.FolderExists(strPath) Then
            bExist = False
            strMessage = "Mappe finnes ikke"
        End If
        If (Not bExist) And bMakePath Then
            bExist = MakePath(strPath)
            'bExist = True
        End If
    Else
        If Not FSO.FileExists(strRootPath & strPath) Then
            bExist = False
            strMessage = "Filen finnes ikke"
        End If
    End If

    
Exit_err:
    If Not bExist Then
        arrTrans(intJobNr).strErrStatus = STR_FEIL
        SkrivErr strFuncName, strMessage, intJobNr, strRootPath & strPath
    End If
        
    Exit Sub
    
Err_Handler:
    strMessage = Err.Description
    bExist = False
    Resume Exit_err

End Sub

Public Function GetFileBase(strXmlFile As String, Optional bKeepExt As Boolean) As String
    Dim strTemp As String
    
    strTemp = Mid(strXmlFile, InStrRev(strXmlFile, "\") + 1)
    If Not bKeepExt Then
        strTemp = Mid(strTemp, 1, InStr(strTemp, ".") - 1)
    End If
    GetFileBase = strTemp
End Function

Public Sub SkrivLog(strMelding As String, Optional strInnFil As String, Optional strUtfil As String)
    On Error GoTo Err_Handler
    
    strMelding = Now() & ": " & strMelding
    If strUtfil <> "" Then
        strMelding = strMelding & " Fra: " & strInnFil & " -> " & strUtfil
    ElseIf strInnFil <> "" Then
        strMelding = strMelding & " Fra: " & strInnFil
    End If
    
    SkrivUtLog strMelding, LOG_PATH
    
Exit Sub
Err_Handler:
    Form1.txtLog = Now & ": 'SkrivLog' Feilet: " & Err.Number & ": " & Err.Description
End Sub

Public Sub SkrivErr(strFuncName As String, strMelding As String, Optional intJobNr As Integer, Optional strInnFil As String, Optional strUtfil As String)
    On Error GoTo Err_Handler
    
    strMelding = Replace(strMelding, vbCrLf, "")
    strMelding = "FEIL: " & Now() & ": i jobbnr: " & intJobNr & ": i proc: " & strFuncName & ": " & strMelding
    
    If strInnFil <> "" Then
        strMelding = strMelding & ": " & strInnFil
    ElseIf strUtfil <> "" Then
        strMelding = strMelding & " Fra: " & strInnFil & " -> " & strUtfil
    End If
   
    If intJobNr > 0 Then
        arrTrans(intJobNr).strLastError = strMelding
    End If

    SkrivUtLog strMelding, LOG_PATH
    SkrivUtLog strMelding, ERR_PATH

Exit Sub
Err_Handler:
    Form1.txtLog = Now & ": 'SkrivErr' Feilet: " & Err.Number & ": " & Err.Description
End Sub

Public Sub SkrivUtLog(strMelding As String, strLogType As String)
    On Error GoTo Err_Handler
    Dim FileNumber As Integer
    Dim strLogFile As String
    
    FileNumber = FreeFile
    Select Case strLogType
    Case LOG_PATH
        Form1.txtLog = strMelding & vbCrLf & Form1.txtLog
        strLogFile = recGeneral.strRootPath & "\" & strLogType & "\log_dag_" & Format(Date, "dd") & ".log"
    Case ERR_PATH
        strLogFile = recGeneral.strRootPath & "\" & strLogType & "\Error_" & Format(Date, "yyyy-mm-dd") & ".log"
    End Select
    
    Open strLogFile For Append As #FileNumber   ' Create file name.
    Print #FileNumber, strMelding
    Close #FileNumber   ' Close file.
Exit Sub
Err_Handler:
    Form1.txtLog = Now & ": 'SkrivUtLog' Feilet: " & Err.Number & ": " & Err.Description
End Sub


Public Function ByttDato(strTekst As String) As String
    Dim intStart As Long
    Dim intSlutt As Long
    Dim intLen As Long
    Dim strTemp As String
    Dim strDato As String
    Dim strOrigDato As String
    Dim strNitfDato As String
    Dim strForran As String
    Dim strEtter As String
    Dim strDateIssue As String
    Dim strVisDato As String
    
    intLen = Len("<timestamp><![CDATA[")
    
    intStart = InStr(1, strTekst, "<timestamp><![CDATA[")
    
    If intStart > 0 Then
        intSlutt = InStr(intStart, strTekst, "]]></timestamp>")
        strOrigDato = Mid(strTekst, intStart + intLen, intSlutt - intStart - intLen)
        strDato = Format(strOrigDato, "yyyymmdd hh.mm.ss")
        strNitfDato = Format(strOrigDato, "yyyymmddThhmmss")
        strDateIssue = Format(strOrigDato, "yyyymm")
        strVisDato = Format(strOrigDato, "dd.mm.yyyy hh:mm")
        
        strForran = Mid(strTekst, 1, intStart - 1)
        strEtter = Mid(strTekst, intSlutt + 15)
        strTemp = strForran & _
            "<timestamp>" & strDato & "</timestamp>" & vbCrLf & _
            "<timenitf>" & strNitfDato & "</timenitf>" & vbCrLf & _
            "<date.issue>" & strDateIssue & "</date.issue>" & vbCrLf & _
            "<visdato>" & strVisDato & "</visdato>" & vbCrLf & _
            "<year>" & Format(strOrigDato, "yyyy") & "</year>" & _
            strEtter
        ByttDato = strTemp
    Else
        ByttDato = strTekst
    End If
    
End Function


Public Function SoundFile(intJobNr As Integer, GlobLokal As typGlob) As Boolean
'Public Sub SoundFile(intJobNr As Integer, strXmlFile As String)
    On Error GoTo Err_Handler
    '*** Kopierer Lydfiler ut fra innhold i XML-fil
    Dim strMessage As String
    Dim bOK As Boolean
    Dim strInFile As String
    Dim strOutFile As String
    Dim f As File
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlNode As IXMLDOMNode

    '*** Henter xml-elementer (tag-innhold) eller attributter fra entydig noder:
    With arrTrans(intJobNr)
            
        Set xmlNode = .xmlDoc.selectSingleNode("/message/soundfile")
    
        If Not xmlNode Is Nothing Then
            strInFile = xmlNode.Text
            
            '*** NB! Temporert hardkodet Path:
            strInFile = Replace(strInFile, "\\Filprint\NTB_DirekteRed\", "Z:\")
            
            Set f = FSO.GetFile(strInFile)
            strOutFile = .strOutPath & "\" '& GetFileBase(strInFile, True)
            
            f.Copy strOutFile, True
            SkrivLog "JobNr:" & Format(Str(intJobNr), "00") & " " & .strType, strInFile, strOutFile
            SoundFile = True
        Else
            SoundFile = False
        End If
    
    End With
    
    Set xmlNode = Nothing
    'Set xmlDoc = Nothing
    
Exit Function
Err_Handler:
    SkrivErr "SoundFile", Err.Description, intJobNr, strOutFile
        
End Function

Public Function MakePath(strPath As String) As Boolean
    On Error GoTo Err_Handler
    If Not FSO.FolderExists(strPath) Then
        FSO.CreateFolder (strPath)
    End If
    MakePath = True
    Exit Function
    
Err_Handler:
    MakePath = False
End Function

Public Sub StartEdit(strPath As String, strFile As String)
    On Error GoTo Err_Handler
    
    strFile = Trim(strFile)
    If strFile = "" Then Exit Sub
    
    Shell recGeneral.strEditor & " " & strPath & strFile
    
    Exit Sub
Err_Handler:
    MsgBox Err.Description
End Sub

Public Function SaveFile(strFilnavn As String, strTekst As String, intJobNr As Integer) As Boolean
    On Error GoTo Err_Handler
    Dim intFilNr  As Integer
    
    intFilNr = FreeFile
    
    Open strFilnavn For Output As #intFilNr
    Print #intFilNr, strTekst
    Close #intFilNr
    SaveFile = True
    Exit Function
    
Err_Handler:
    SaveFile = False
    SkrivErr "SaveFile", Err.Description, intJobNr, strFilnavn
End Function

Public Function LoadFile(intJobNr As Integer, strFilnavn As String, GlobLokal As typGlob, intType As enuType) As Boolean
    On Error GoTo Err_Handler
    Dim bTest As Boolean
                                
    LoadFile = True
    
    With GlobLokal
        If intType = typText Then
            If Not .bLoaded Then
                bTest = LoadTxtFile(strFilnavn, .strText, strMessage)
            ElseIf .bLoaded And .Type = typXmlDoc Then
                .strText = arrTrans(intJobNr).xmlDoc.xml
                bTest = True
            Else
                Exit Function
            End If
            
            If bTest Then
                .Type = typText
                .bLoaded = True
            Else
                SkrivErr "LoadFile", strMessage, intJobNr, strFilnavn
                LoadFile = False
            End If
        ElseIf intType = typXmlDoc Then
            If Not .bLoaded Then
                bTest = LoadXmlDoc(arrTrans(intJobNr).xmlDoc, strFilnavn, True, strMessage, True)
            ElseIf .bLoaded And .Type = typText Then
                bTest = LoadXmlDoc(arrTrans(intJobNr).xmlDoc, .strText, False, strMessage, True)
            Else
                Exit Function
            End If
            
            If bTest = True Then
                .Type = typXmlDoc
                .bLoaded = True
            Else
                SkrivErr "LoadFile", strMessage, intJobNr, strFilnavn
                LoadFile = False
            End If
        Else
            LoadFile = False
            Exit Function
        End If
    End With
    
    
    Exit Function
Err_Handler:
    LoadFile = False
    SkrivErr "LoadFile", Err.Description, intJobNr, strFilnavn
End Function

Public Function LoadTxtFile(strFilnavn As String, strText As String, strMessage As String) As Boolean
    On Error GoTo Err_Handler
    Dim intFilNr As Integer
    Dim strLine As String
    
    intFilNr = FreeFile
    
    Open strFilnavn For Input As #intFilNr
    
    strText = ""
    Do Until EOF(intFilNr)
        Line Input #intFilNr, strLine
        strText = strText & strLine & vbCrLf
    Loop
    
    Close #intFilNr
    LoadTxtFile = True
    Exit Function
    
Err_Handler:
    LoadTxtFile = False
    strMessage = Err.Description
End Function

Public Sub VisDebug(intJobNr As Integer, intType As Integer, strInn As String, strUt As String, lpIndex As Long)
    
    Static strInnSave As String
    
    Select Case intType
        
    Case 1
        strInnSave = strInn
    Case 2
        FormDebug.lpIndex = lpIndex
        FormDebug.txtInn = strInnSave
        FormDebug.txtUt = strUt
        FormDebug.intJobNr = intJobNr
        FormDebug.Show vbModal
        'MsgBox "Vis neste"
    End Select
       
End Sub

Sub test()

    Dim FSO As Scripting.FileSystemObject
    
    Set FSO = New Scripting.FileSystemObject
    Dim strInnFil, strTekst
    Dim objInnFil As Scripting.TextStream
    
    strInnFil = "d:\sms_meldinger\test_nitf2.xml"
    Set objInnFil = FSO.OpenTextFile(strInnFil, ForReading)
    strTekst = objInnFil.ReadAll

End Sub

