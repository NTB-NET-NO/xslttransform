VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormXmlCopy 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dialog Caption"
   ClientHeight    =   5565
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   8100
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5565
   ScaleWidth      =   8100
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSlett 
      Caption         =   "&Slett"
      Height          =   375
      Left            =   1440
      TabIndex        =   5
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton cmdNy 
      Caption         =   "&Ny"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton cmdLagre 
      Caption         =   "&Lagre"
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   6720
      TabIndex        =   1
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5280
      TabIndex        =   0
      Top             =   5040
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid Grid1 
      Height          =   4815
      Left            =   0
      TabIndex        =   2
      Top             =   120
      Width           =   8055
      _ExtentX        =   14208
      _ExtentY        =   8493
      _Version        =   393216
      Cols            =   6
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "FormXmlCopy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public lpCopyIndex As Long
Dim lpStart As Long

Private Sub cmdAvbryt_Click()
    Unload Me
End Sub

Private Sub cmdLagre_Click()
    SaveXmlCopy
End Sub

Private Sub cmdNy_Click()
'    Me.Grid1.Row = Me.Grid1.Rows - 1
'    Me.Grid1.Col = 1
'    FormXmlCopyDialog.lpPrevNode = Me.Grid1.Text
'    FormXmlCopyDialog.lpCopyIndex = 0

    FormXmlCopyDialog.intRow = Me.Grid1.Rows
    FormXmlCopyDialog.lpCopyIndex = 0
    FormXmlCopyDialog.Show vbModal
End Sub

Private Sub cmdOK_Click()
    '*** sett inn kode her
    Unload Me
End Sub

Private Sub Form_Load()
    Dim lpEnd As Long
    Dim lpIndex As Long
    Dim intRow As Integer
    
    With arrTrans(intJobIndex)
        lpStart = .lpCopyStart
        lpEnd = .lpCopyEnd
    End With
    
    Me.Grid1.Row = 0
    Me.Grid1.ColWidth(0) = 300
    Me.Grid1.ColWidth(1) = 500
    Me.Grid1.ColWidth(2) = 2500
    Me.Grid1.ColWidth(3) = 2500
    Me.Grid1.ColWidth(4) = 500
    Me.Grid1.ColWidth(5) = 500
    Me.Grid1.Col = 0
    Me.Grid1.Text = "Nr"
    Me.Grid1.Col = 1
    Me.Grid1.Text = "Copy type"
    Me.Grid1.Col = 2
    Me.Grid1.Text = "Ut path"
    Me.Grid1.Col = 3
    Me.Grid1.Text = "Xpath"
    Me.Grid1.Col = 4
    Me.Grid1.Text = "Compare"
    Me.Grid1.Col = 5
    Me.Grid1.Text = "Hit"
    
    lpIndex = lpStart
    Do
        intRow = intRow + 1
        VisEnRad intRow, lpIndex
    Loop Until lpIndex = 0
    
    Me.Grid1.Row = 1
    Me.Grid1.Col = 1
    Me.Grid1.ColSel = 4
End Sub

Sub VisEnRad(intRow As Integer, lpIndex As Long)
    If intRow > Me.Grid1.Rows - 1 Then
        Me.Grid1.AddItem ("")
    End If
    Me.Grid1.Row = intRow
        
    With arrXmlCopy(lpIndex)
        Me.Grid1.RowData(intRow) = lpIndex
        Me.Grid1.Col = 0
        Me.Grid1.Text = Format(intRow, "00")
        Me.Grid1.Col = 1
        Me.Grid1.Text = .strType
        Me.Grid1.Col = 2
        Me.Grid1.Text = .strPath
        Me.Grid1.Col = 3
        Me.Grid1.Text = .strFind
        Me.Grid1.Col = 4
        Me.Grid1.Text = .strCompare
        Me.Grid1.Col = 5
        Me.Grid1.Text = .intHit
        
        lpIndex = .lpNexNode
    End With
End Sub

Private Sub Grid1_dblClick()
    FormXmlCopyDialog.lpCopyIndex = Me.Grid1.RowData(Me.Grid1.Row)
    FormXmlCopyDialog.intRow = Me.Grid1.Row
    FormXmlCopyDialog.Show vbModal
End Sub

Private Sub Grid1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Grid1_dblClick
    End If
End Sub

Private Sub SaveXmlCopy()
    Dim xmlDoc As DOMDocument
    Dim lpIndex As Long
    Dim strXml As String
    Dim strFileName As String
    
    strXml = XML_HEAD & vbCrLf
    strXml = strXml & LagXmlTag("copyxml", "", , 1)
    lpIndex = lpStart
    
    Do
        With arrXmlCopy(lpIndex)
                    
            Select Case .strType
            Case "file"
                strXml = strXml & LagXmlTag(.strType, "", "path=" & .strPath)
            Case "xpath"
                strXml = strXml & LagXmlTag(.strType, "", "path=" & .strPath & ";compare=" & .strCompare & ";hit=" & .intHit, 1)
                strXml = strXml & LagXmlTag("find", .strFind, "")
                strXml = strXml & LagXmlTag(.strType, "", , 2)
            End Select
            lpIndex = .lpNexNode
        End With
        
    Loop Until lpIndex = 0
    
    strXml = strXml & LagXmlTag("copyxml", "", , 2)

    'InputBox "", "", strXml
    
    If Not LoadXmlDoc(xmlDoc, strXml, False, strMessage, True) Then
        MsgBox strMessage
        Exit Sub
    End If
        
    strFileName = recGeneral.strRootPath & "\copy\" & arrTrans(intJobIndex).strCopyFile
    'xmlDoc.save "c:\testXmlCopySave.xml"
    If FSO.FileExists(strFileName & ".bak") Then
        FSO.DeleteFile strFileName & ".bak"
    End If
    If FSO.FileExists(strFileName) Then
        FSO.MoveFile strFileName, strFileName & ".bak"
    End If
    xmlDoc.save strFileName
    Set xmlDoc = Nothing
    
    'SaveFile "c:\testXCHGsave.xml", strXml
    
End Sub


