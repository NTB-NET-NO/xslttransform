Attribute VB_Name = "OldFunctions"
Option Explicit

'Public intDimCopyXml As Integer
'Public arrCopyXml() As CopyXmlRec
'Type CopyXmlRec
'    strXPath    As String
'    strPath     As String
'    bExist      As Boolean
'End Type
'

Public Function Old_CheckPath(strXsltFile As String, strOutPath As String, strInPath As String, strMessage As String) As Boolean
    On Error GoTo Err_Handler
    Dim FSO As FileSystemObject
    
    Set FSO = New FileSystemObject
    
    'CheckPath = True
    If strXsltFile <> "" Then
        If Not FSO.FileExists(recGeneral.strRootPath & "\" & strXsltFile) Then
            strMessage = "Transformasjons-fil finnes ikke! : " & recGeneral.strRootPath & "\" & strXsltFile
            'CheckPath = False
        End If
    End If
    If strOutPath <> "" Then
        If Not FSO.FolderExists(strInPath) Then
            strMessage = "Inn path finnes ikke! : " & strInPath
            'CheckPath = False
        End If
    End If
    
    MakePath strOutPath
    
    Set FSO = Nothing
    
Exit_err:
    Exit Function
    
Err_Handler:
    strMessage = Err.Description & ". For Path: " & strOutPath
    Resume Exit_err
End Function



Public Function old_InitCopyXml(strXmlFile As String, intCopyXmlLines As Integer, strMessage As String) As Boolean
    Dim strInFile As String
    Dim strOutFile As String
    Dim f As File
    Dim xmlDoc As DOMDocument
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlNode As IXMLDOMNode
    Dim i As Integer

    intCopyXmlLines = 0
    Set xmlDoc = New DOMDocument
    If Not LoadXmlDoc(xmlDoc, strXmlFile, True, strMessage) Then
        SkrivErr "InitCopyXml", strMessage, "", strXmlFile
        old_InitCopyXml = False
        Exit Function
    End If
    
''    intDimCopyXml = intDimCopyXml + 1
''    ReDim Preserve arrCopyXml(1 To MAX_COPY, intDimCopyXml)
'
'    '*** Henter xml-elementer (tag-innhold) fra noder:
'    Set xmlNodeList = xmlDoc.selectNodes("/copyxml/dest")
'
'    If Not xmlNodeList Is Nothing Then
'        For i = 0 To xmlNodeList.length - 1
'            With arrCopyXml(i + 1, intDimCopyXml)
'                .strXPath = xmlNodeList.Item(i).selectSingleNode("xpath").Text
'                'strInFile = xmlNodeList.Item(i).childNodes.Item(1).Text
'                .strPath = xmlNodeList.Item(i).selectSingleNode("path").Text
'
'                Set xmlNode = xmlNodeList.Item(i).selectSingleNode("exist")
'                If Not xmlNode Is Nothing Then
'                    .bExist = xmlNode.Text <> "not"
'                Else
'                    .bExist = True
'                End If
'
'            End With
'        Next
'        intCopyXmlLines = i
'        old_InitCopyXml = True
'        Set xmlNode = Nothing
'        Set xmlNodeList = Nothing
'    Else
'        old_InitCopyXml = False
'    End If
    
    Set xmlDoc = Nothing

End Function

Public Sub Old_Init()

'    Dim bOK As Boolean
'    Dim intJobNr As Integer
'    Ini_File = CurDir & "\" & INI_FILE_NAME
'    With recGeneral
'        '*** Initialiserer generelle varibler for programmet
'        If HentIniStreng(Ini_File, "General", "GUI", "") <> "no" Then
'            .bGui = True
'        End If
'        .strRootPath = HentIniStreng(Ini_File, "General", "RootPath", "")
'        'strSpoolPath = HentIniStreng(Ini_File, "General", "SpoolPath", "")
'        .strSoundPath = HentIniStreng(Ini_File, "General", "SoundPath", "")
'        .strSoundXPath = HentIniStreng(Ini_File, "General", "SoundXPath", "")
'        .strSoundXMessage = HentIniStreng(Ini_File, "General", "SoundXMessage", "")
'        .bTimerEnabled = HentIniStreng(Ini_File, "General", "StartTimer", "") = "yes"
'        .intInterval = Val(HentIniStreng(Ini_File, "General", "Interval", "")) * 1000
'    End With
'
'    For intJobNr = 1 To MAX_TRANS
'        '*** Fyller array fra Ini-fila
'        ReDim Preserve arrTrans(1 To intJobNr)
'        bOK = FyllTransRec(arrTrans(intJobNr), intJobNr)
'        If Not bOK Then
'            Exit For
'        End If
'
'        '*** Sjekker at filer og Path finnes:
'        CheckPath intJobNr
'
'        With arrTrans(intJobNr)
'
'            Select Case .strType
'            Case "xslt"
'                If Not InitXslt(recGeneral.strRootPath & "\xslt\" & .strXsltFile, .xmlDoc, .xsltProc, strMessage) Then
'                    MsgBox strMessage
'                    .strErrStatus = STR_FEIL
'                End If
'            Case "copy"
'                If .strXsltFile <> "" Then
'                    '*** Initier CopyXml tabell for denne jobben
'                    If Not InitCopyXml(recGeneral.strRootPath & "\copy\" & .strCopyFile, .intXchgLines, strMessage) Then
'                        MsgBox strMessage
'                        .strErrStatus = STR_FEIL
'                    Else
'                        .intDim = intDimCopyXml
'                    End If
'                End If
'            Case "xchg", "delete", "xmlrtf", "valg", "lyd", "mail"
'            Case Else
'                strMessage = "Ukjent transformasjons-type! " & .strType
'
'                MsgBox strMessage
'                .strErrStatus = STR_FEIL
'            End Select
'        End With
'        DoEvents
'    Next
'    intTransAnt = intJobNr - 1
'    Ini_File = INI_FILE_XML
'
End Sub

Private Function old_FinnXmlXpath(n As Integer, intDim As Integer, xmlDoc As DOMDocument, strOutPath2 As String) As Boolean
'    Dim xmlNode As IXMLDOMNode
'
'    With arrCopyXml(n, intDim)
'        '.strXPath = "/message/fields/field[name='LydFilnavn']/value"
'        Set xmlNode = xmlDoc.selectSingleNode(.strXPath)
'        'MsgBox xmlDoc.xml
'        If xmlNode Is Nothing Then
'            strOutPath2 = .strPath
'            FinnXmlXpath = Not .bExist
'        Else
'            If Trim(xmlNode.Text) = "" Then
'                strOutPath2 = .strPath
'                FinnXmlXpath = Not .bExist
'            Else
'                strOutPath2 = .strPath
'                FinnXmlXpath = .bExist
'            End If
'        End If
'    End With
End Function


Public Sub old_XchgTransform(i As Integer, strXmlFile As String, intXchgLines As Integer)
    '*** Gammel XCHG:
    Dim strMessage As String
    Dim bOK As Boolean
    Dim strOutFile As String
    Dim strText As String
    Dim intFileNr As Integer
    Dim strLine As String
    
    With arrTrans(i)
    
        If Not FileArchive(.strArchive, strXmlFile) Then
            Exit Sub
        End If
           
        intFileNr = FreeFile
        Open strXmlFile For Input As intFileNr
        strText = ""
        Do Until EOF(intFileNr)
            Line Input #intFileNr, strLine
            strText = strText & strLine & vbCrLf
        Loop
        Close intFileNr
    
        '*** Gammel XCHG:
        strText = doReplace(strText, intXchgLines)
        
        strOutFile = .strOutPath & "\" & GetFileBase(strXmlFile) & "." & .strFileExt

        Set tstreamUt = FSO.OpenTextFile(strOutFile, ForWriting, True)
        tstreamUt.Write (strText)
    
        tstreamUt.Close
        Set tstreamUt = Nothing
        
        SkrivLog "JobNr:" & .strNr & " " & .strType, strXmlFile, strOutFile
    End With
End Sub

Private Sub old_DoJobs()
'    On Error GoTo Err_Handler
'    Dim strInnFile As String
'    Dim strPathFile As String
'    Dim strOutFile As String
'    Dim intJobNr As Integer
'    Dim intLines As Integer
'
'    For intJobNr = 1 To intTransAnt
'        With arrTrans(intJobNr)
'        If .strStatus = STR_OK And .strErrStatus <> STR_FEIL Then
'
'            'JobRunning intJobNr, True
'
'            DoEvents
'
''            If Right(.strXchgFile, 3) = "txt" Then
''            'If .strXchgFile <> "" Then
''                '*** Initier xchange tabell for denne jobben
''                If Not InitXchg(recGeneral.strRootPath & "\xchg\" & .strXchgFile, intLines, strMessage) Then
''                    MsgBox strMessage
''                End If
''            End If
'
'            strInnFile = Dir(.strInPath & "\" & .strFilter)
'
'            If .strErrStatus = STR_VARSEL Then
'                .strErrStatus = ""
'                SkrivEnJobb intJobNr
'            End If
'            Do Until strInnFile = ""
'            '*** For hver input-fil
'                strPathFile = .strInPath & "\" & strInnFile
'
'
'                '*** Sjekker og eventuelt setter fil-attributt:
'                If Not FileArchive(.strArchive, strPathFile) Then
'                    GoTo Skip_File
'                End If
'
'                '*** XCHG, bytter tegn:
'                If .lpXchgStart > 0 Then
'                    If LoadTxtFile(strPathFile, Glob.strText) Then
'                        DoTxtXchg Glob.strText, intJobNr
'
''                            If Not LoadXmlDoc(Glob.xmlDoc, Glob.strText, False, strMessage, True) Then
''                                 SkrivErr "DoJobs", strMessage, intJobNr, strPathFile
''                            End If
''                                 DoXmlXchg Glob.xmlDoc, intJobNr
''                                 Glob.strText = Glob.xmlDoc.xml
''                            Else
'
'                    End If
'
'                    'Glob.xmlDoc.save .strOutPath & "\" & GetFileBase(strInnFile) & "." & .strFileExt
'
'                    strOutFile = .strOutPath & "\" & GetFileBase(strInnFile) & "." & .strFileExt
'                    SaveFile strOutFile, Glob.strText
'                    SkrivLog "JobNr:" & intJobNr & " " & .strType, strPathFile, strOutFile
'                End If
'
'                'XSLTransform intJobNr, strPathFile, intLines
'                'XchgTransform intJobNr, strPathFile, intLines 'last
'                'CopyFile intJobNr, strPathFile, .intXchgLines
'                DeleteFile intJobNr, strPathFile
'
'                Select Case .strType
''                Case "trans"
'
'                    'XchgTransform intJobNr, strPathFile, intLines 'first
'                    'XSLTransform intJobNr, strPathFile, intLines
'                    'XchgTransform intJobNr, strPathFile, intLines 'last
'                    'CopyFile intJobNr, strPathFile, .intXchgLines
'                    'DeleteFile intJobNr, strPathFile
''                Case "xslt"
''                    XSLTransform intJobNr, strPathFile, intLines
''                    DeleteFile intJobNr, strPathFile
''                Case "xchg"
''                Case "copy"
''                    CopyFile intJobNr, strPathFile, .intXchgLines
''                    DeleteFile intJobNr, strPathFile
'                Case "xmlrtf"
'                    XMLRtfTransform intJobNr, strPathFile, intLines
'                    DeleteFile intJobNr, strPathFile
'                Case "mail"
'                    'MailFile intJobNr, strPathFile
'                    'DeleteFile intJobNr, strPathFile
'                Case "lyd"
'                    SoundFile intJobNr, strPathFile
'                    DeleteFile intJobNr, strPathFile
'                Case "valg"
'                    ValgTransform intJobNr, strPathFile
'                    DeleteFile intJobNr, strPathFile
'                Case "delete"
'                    DeleteFile intJobNr, strPathFile
'                Case Else
'                End Select
'Skip_File:
'                strInnFile = Dir
'                DoEvents
'            Loop
'            JobRunning intJobNr, False
'        End If
'        End With
'    Next
'
'Exit Sub
'Err_Handler:
'    SkrivErr "DoJobs", Err.Description, intJobNr, strPathFile
'    arrTrans(intJobNr).strErrStatus = STR_VARSEL
'    SkrivEnJobb intJobNr
End Sub


Public Sub old_XSLTransform(intJobNr As Integer)
'Public Sub XSLTransform(intJobNr As Integer, strXmlFile As String, intXchgLines As Integer)
    Dim strMessage As String
    Dim bOK As Boolean
    Dim strOutFile As String
    Dim intFile As Integer
            
    Dim strResultText As String
    
    With arrTrans(intJobNr)
        
        'If Not LoadXmlDoc(.xmlDoc, strXmlFile, True, strMessage) Then
        '    SkrivErr "XSLTransform", strMessage, intJobNr, strXmlFile
        'End If
        
        .xsltProc.Transform
        strResultText = .xsltProc.output
                

        'strOutFile = .strOutPath & "\" & GetFileBase(strXmlFile) & "." & .strFileExt

        'intFile = FreeFile
        'Open strOutFile For Output As #intFile
        'Print #intFile, strResultText
        'Close #intFile
        
        'SkrivLog "JobNr:" & .strNr & " " & .strType, strXmlFile, strOutFile
    End With
        
End Sub

Public Sub old_XMLRtfTransform(i As Integer, strInFile As String, intXchgLines As Integer)
    '*** Transformering av XML-Filer med RTF-kode fra Distributor
    Dim strMessage As String
    Dim bOK As Boolean
    Dim bSkipFile As Boolean
    Dim strOutFile As String
            
    Dim strResultText As String
    
    With arrTrans(i)
    
        'strResultText = StripXml(strInFile, bSkipFile)
        
        If bSkipFile Then
            CopyError i, strInFile
            Exit Sub
        End If
        
'        '*** Hvis denne jobben har xchg-fil for erstatting av strenger:
'        If .strXchgFile <> "" Then
'            strResultText = doReplace(strResultText, intXchgLines)
'        End If
        
'        strOutFile = .strOutPath & "\" & GetFileBase(strInFile) & "." & .strFileExt
'
'        Open strOutFile For Output As #1
'        Print #1, strResultText
        
'        Close #1
        
'        SkrivLog "JobNr:" & .strNr & " " & .strType, strInFile, strOutFile
    End With
        
End Sub


Public Sub old_ValgTransform(intJobNr As Integer, strInFile As String)
    '*** Transformering av Valg-Filer av typen Kvasi-tagger til XML-format
    Dim strMessage As String
    Dim bOK As Boolean
    Dim strOutFile As String
    Dim intFil As Integer
            
    Dim strResultText As String
    
    With arrTrans(intJobNr)
        If Not FileArchive(.strArchive, strInFile) Then
            Exit Sub
        End If
        
        strResultText = ConvertValg(strInFile, intJobNr)

        strOutFile = .strOutPath & "\" & GetFileBase(strInFile) & "." & .strFileExt

        intFil = FreeFile
        Open strOutFile For Output As #intFil
        Print #intFil, strResultText
        
        Close #intFil
        
        SkrivLog "JobNr:" & .strNr & " " & .strType, strInFile, strOutFile
    End With
        
End Sub

Public Sub old_CopyFile(intJobNr As Integer, strXmlFile As String, intCopyXmlLines As Integer)
   ' On Error GoTo Err_Handler
    Dim strMessage As String
    Dim bOK As Boolean
    Dim strInFile As String
    Dim strOutFile As String
    Dim strOutPath2 As String
    Dim f As File
    Dim intDay As Integer
    Dim n As Integer
    Dim n0 As Integer
    Dim xmlDoc As DOMDocument
    
    'strOutFile = GetFileBase(strXmlFile, True)
                
     With arrTrans(intJobNr)
         If .lpCopyStart = 0 Then
             '*** Enkel filkopiering:
             strOutFile = .strOutPath & "\" '& strOutFile
             If FileArchive(.strArchive, strXmlFile, f) Then
                 f.Copy strOutFile, True
                 SkrivLog "JobNr:" & Format(Str(intJobNr), "00") & " " & .strType, strXmlFile, strOutFile
             End If
         Else
             '*** Kompleks multiple filkopiering:
             'xmlCopyFiles strXmlFile, intJobNr
         End If
         
    End With

Exit Sub
Err_Handler:
    SkrivErr "CopyFile", Err.Description, intJobNr, strXmlFile, strOutFile

End Sub


