Attribute VB_Name = "IniModule"
Option Explicit

Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Public Sub InitXml()
    On Error GoTo Err_Handler
    Dim bOK As Boolean
    Dim intJobNr As Integer
    Dim xmlNode As IXMLDOMNode
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlDocIni As DOMDocument
    
    MakePath strCurDir & "\backup"
    
    If Not LoadXmlDoc(xmlDocIni, Ini_File, True, strMessage, True) Then
        MsgBox strMessage
    End If
   
    Set xmlNode = xmlDocIni.selectSingleNode("/init/head")
    
    With recGeneral
        '*** Initialiserer generelle varibler for programmet
        .strTitle = HentXmlNodeText(xmlNode, "Title", "")
        .bGui = HentXmlNodeText(xmlNode, "Gui", "") <> "No"
        .strRootPath = HentXmlNodeText(xmlNode, "RootPath", "")
        'strSpoolPath = HentXmlNodeText(xmlNode, "SpoolPath", "")
        'strIni = strIni & LagXmlTag("Editor", .strEditor)
        .strEditor = HentXmlNodeText(xmlNode, "Editor", "")
        .strSoundPath = HentXmlNodeText(xmlNode, "SoundPath", "")
        .strSoundXPath = HentXmlNodeText(xmlNode, "SoundXPath", "")
        .strSoundXMessage = HentXmlNodeText(xmlNode, "SoundXMessage", "")
        .bTimerEnabledStart = HentXmlNodeText(xmlNode, "TimerEnabled", "") = "True"
        .intInterval = Val(HentXmlNodeText(xmlNode, "Interval", ""))
    
        '*** Lager standardmapper:
        MakePath .strRootPath & "\COPY"
        MakePath .strRootPath & "\XCHG"
        MakePath .strRootPath & "\XSLT"
        MakePath .strRootPath & "\LOG"
        MakePath .strRootPath & "\ERROR"
        MakePath .strRootPath & "\spool"
    
    End With
    
    Set xmlNodeList = xmlDocIni.selectNodes("/init/jobs/job")

    ReDim Preserve arrXmlXchg(0)
    ReDim Preserve arrXchgReplace(0)
    ReDim Preserve arrXmlCopy(0)

    For intJobNr = 1 To xmlNodeList.length
        '*** Fyller array fra Ini-fila
        ReDim Preserve arrTrans(1 To intJobNr)
        bOK = InitTransRecXml(intJobNr, xmlNodeList.Item(intJobNr - 1))
        If Not bOK Then
            Exit For
        End If
        
        CheckPath intJobNr
        
        With arrTrans(intJobNr)
            
            InitXmlXchg intJobNr
            
            If .strCopyFile <> "" Then
                InitXmlCopy intJobNr
            End If
            
            If .strXsltFile <> "" Then
                If Not InitXslt(recGeneral.strRootPath & "\xslt\" & .strXsltFile, .xmlDoc, .xsltProc, strMessage) Then
                    SkrivErr "IniXml", strMessage, intJobNr, "xslt\" & .strXsltFile
                    .strErrStatus = STR_FEIL
                End If
            End If
        
            Select Case .strType
            Case "trans", "xslt", "copy", "xchg", "delete", "xmlrtf", "valg", "lyd", "mail"
                '*** Do Nothing
            Case Else
                strMessage = "Ukjent transformasjons-type! " & .strType
                
                MsgBox strMessage
                .strErrStatus = STR_FEIL
            End Select
        
        End With
        DoEvents

    Next
    
    
    intTransAnt = intJobNr - 1
    
    Set xmlDocIni = Nothing
Exit Sub

Err_Handler:

    MsgBox Err.Description
End Sub

Public Function HentIniStreng(strIniFil As String, strAppl As String, strKey As String, strDefault As String) As String
    Dim lpReturnedString As String
    Const nSize = 255
    Dim lpReturn As Long
    
    lpReturnedString = String(nSize, Chr(0))
        
    '(ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
    lpReturn = GetPrivateProfileString(strAppl, strKey, strDefault, lpReturnedString, nSize, strIniFil)
    HentIniStreng = Left(lpReturnedString, InStr(1, lpReturnedString, Chr(0)) - 1)
    
End Function

Public Sub LagreIni()
    On Error GoTo Err_Handler
    Dim strIni As String
    Dim xmlDoc As DOMDocument
    Dim i As Integer
    Set xmlDoc = New DOMDocument
    
    strIni = XML_HEAD & vbCrLf
    strIni = strIni & "<init>" & vbCrLf
    strIni = strIni & LagIniHead
    
    strIni = strIni & "<jobs>" & vbCrLf
    For i = 1 To intTransAnt
        strIni = strIni & LagIniJob(i)
    Next
    strIni = strIni & "</jobs>" & vbCrLf
    
    strIni = strIni & "</init>" & vbCrLf
    
    If Not LoadXmlDoc(xmlDoc, strIni, False, strMessage, True) Then
        MsgBox strMessage
        Exit Sub
    End If
        
    FSO.MoveFile Ini_File, "backup/" & Format(Now, "yyyy-mm-dd_hh-mm-ss_") & INI_FILE_XML
    xmlDoc.save Ini_File
    
    Set xmlDoc = Nothing
    Exit Sub
Err_Handler:
    MsgBox Err.Description
End Sub

Public Function LagIniHead() As String
    Dim strIni As String
    
    strIni = strIni & "<head>" & vbCrLf
    With recGeneral
        strIni = strIni & LagXmlTag("Title", .strTitle)
        strIni = strIni & LagXmlTag("Gui", LTrim(Str(.bGui)))
        strIni = strIni & LagXmlTag("TimerEnabled", LTrim(Str(.bTimerEnabledStart)))
        strIni = strIni & LagXmlTag("Interval", LTrim(Val(.intInterval)))
        strIni = strIni & LagXmlTag("RootPath", .strRootPath)
        strIni = strIni & LagXmlTag("Editor", .strEditor)
        strIni = strIni & LagXmlTag("SoundPath", .strSoundPath)
        strIni = strIni & LagXmlTag("SoundXMessage", .strSoundXMessage)
        strIni = strIni & LagXmlTag("SoundXPath", .strSoundXPath)
        strIni = strIni & LagXmlTag("LoggPath", .strLoggPath)
    End With
    strIni = strIni & "</head>" & vbCrLf
    
    LagIniHead = strIni
End Function

Public Function LagIniJob(intIndex As Integer) As String
    Dim strIni As String
    
    With arrTrans(intIndex)
        strIni = strIni & "<job id=""" & intIndex & """ type=""" & .strType & """>" & vbCrLf
        strIni = strIni & LagXmlTag("Text", .strText)
        strIni = strIni & LagXmlTag("Status", .strStatus)
        strIni = strIni & LagXmlTag("InPath", .strInPath)
        strIni = strIni & LagXmlTag("Filter", .strFilter)
        strIni = strIni & LagXmlTag("OutPath", .strOutPath)
        strIni = strIni & LagXmlTag("FileExt", .strFileExt)
        strIni = strIni & LagXmlTag("TransFile", .strXsltFile)
        strIni = strIni & LagXmlTag("XchgFile", .strXchgFile)
        strIni = strIni & LagXmlTag("CopyFile", .strCopyFile)
        strIni = strIni & LagXmlTag("Delete", .strDelete)
        strIni = strIni & LagXmlTag("DelDays", .strDelDays)
        strIni = strIni & LagXmlTag("Archive", .strArchive)
    End With
    strIni = strIni & "</job>" & vbCrLf
    
    LagIniJob = strIni
End Function

Public Function InitTransRecXml(intJobNr As Integer, xmlNode As IXMLDOMNode) As Boolean
    On Error GoTo Err_Handler
    Dim strTemp As String
    
    If xmlNode Is Nothing Then
        InitTransRecXml = False
        Exit Function
    End If
          
    With arrTrans(intJobNr)
        .strNr = Format(Str(intJobNr), "00")
        .strType = HentXmlNodeText(xmlNode, "@type", "")
        If .strType = "" Then
            InitTransRecXml = False
            Exit Function
        End If

        .strText = HentXmlNodeText(xmlNode, "Text", "")
        .strInPath = HentXmlNodeText(xmlNode, "InPath", "")
        .strFilter = HentXmlNodeText(xmlNode, "Filter", "")
        .strOutPath = HentXmlNodeText(xmlNode, "OutPath", "")
        .strFileExt = HentXmlNodeText(xmlNode, "FileExt", "")
        .strXsltFile = HentXmlNodeText(xmlNode, "TransFile", "")
        .strXchgFile = HentXmlNodeText(xmlNode, "XchgFile", "")
        .strCopyFile = HentXmlNodeText(xmlNode, "CopyFile", "")
        .strDelete = HentXmlNodeText(xmlNode, "Delete", "")
        .strDelDays = HentXmlNodeText(xmlNode, "DelDays", "")
        .strArchive = HentXmlNodeText(xmlNode, "Archive", "")
        .strStatus = HentXmlNodeText(xmlNode, "Status", "")
        .strErrStatus = ""
    End With
    InitTransRecXml = True
    Exit Function
Err_Handler:
    InitTransRecXml = False
    SkrivErr "FyllTransRec", Err.Description, intJobNr
End Function

