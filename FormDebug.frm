VERSION 5.00
Begin VB.Form FormDebug 
   Caption         =   "Debuging av XSLT og XCHG"
   ClientHeight    =   9165
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14040
   LinkTopic       =   "Form2"
   ScaleHeight     =   9165
   ScaleWidth      =   14040
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtBeskriv 
      Height          =   315
      Left            =   1560
      Locked          =   -1  'True
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   120
      Width           =   4575
   End
   Begin VB.TextBox txtType 
      Height          =   315
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   6
      Text            =   "Text1"
      Top             =   120
      Width           =   1335
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   12720
      TabIndex        =   5
      Top             =   8760
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Vis neste"
      Default         =   -1  'True
      Height          =   375
      Left            =   11280
      TabIndex        =   4
      Top             =   8760
      Width           =   1215
   End
   Begin VB.TextBox txtUt 
      Height          =   7935
      Left            =   6240
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   2
      Top             =   720
      Width           =   7695
   End
   Begin VB.TextBox txtInn 
      Height          =   7935
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   720
      Width           =   6015
   End
   Begin VB.Label Label2 
      Caption         =   "Tekst ut:"
      Height          =   255
      Left            =   6240
      TabIndex        =   3
      Top             =   480
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "Tekst inn:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   2415
   End
End
Attribute VB_Name = "FormDebug"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public intJobNr As Integer
Public lpIndex As Long

Private Sub cmdAvbryt_Click()
    arrTrans(intJobNr).bDebug = False
    Unload Me
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    With arrXmlXchg(lpIndex)
        Me.txtType = .strType
        Me.txtBeskriv = .strXpath
    End With
End Sub
