Attribute VB_Name = "ValgModule"
Option Explicit

Public Function ConvertValg(strFil As String, intJobNr As Integer) As String
    On Error GoTo Err_Handler
    Dim strDoc As String
    Dim strFipHeader As String
    Dim strMainPart As String
    Dim strAdmPart As String
    Dim strLine As String
    Dim intFileNr As Integer
    Dim strSisteLinje As String
'    Dim fs As Scripting.FileSystemObject
    Dim f As File
    Dim strFileDate As String

    Dim strFilnavn As String
        
    'Set fs = New Scripting.FileSystemObject
    Set f = FSO.GetFile(strFil)
    strFileDate = f.DateLastModified
    strFilnavn = f.Name
        
    intFileNr = FreeFile
    Open strFil For Input As intFileNr
   
    strDoc = "<?xml version=""1.0"" encoding=""ISO-8859-1"" standalone=""yes""?>" & vbCrLf & _
            "<message>" & vbCrLf
    
    strFipHeader = GetFipHeader(intFileNr, strSisteLinje)
    strMainPart = GetMainPart(intFileNr, strSisteLinje)
    strAdmPart = MakeAdmInfo(strFipHeader, strMainPart, strFileDate, strFilnavn, intJobNr)

    strDoc = strDoc & strAdmPart & strMainPart & "</message>"
    
    Close intFileNr
    ConvertValg = strDoc
    
    Exit Function
Err_Handler:
    

End Function

Private Function GetFipHeader(intFileNr As Integer, strSisteLinje As String) As String
    
    Dim strA As String
    Dim strB As String
    Dim strLine As String
    Dim lngPos As Long
    
    strA = strA & "<fipheader>" & vbCrLf
    strA = strA & "<raw><![CDATA[" & vbCrLf
    
    Line Input #intFileNr, strLine
    strA = strA & strLine & vbCrLf
    Do Until EOF(intFileNr)
        Line Input #intFileNr, strLine
        lngPos = InStr(1, strLine, "~")
        If lngPos > 0 Then Exit Do
        strA = strA & strLine & vbCrLf
    Loop
    
    strA = strA & Left(strLine, lngPos) & vbCrLf
    'strSisteLinje = Mid(strLine, lngPos + 2)
    
    strA = strA & "]]></raw>" & vbCrLf
    
    'strA = strA & strB & vbCrLf
    
    strA = strA & "</fipheader>" & vbCrLf
    
    GetFipHeader = strA
End Function

Private Function GetMainPart(intFileNr As Integer, strSisteLinje As String) As String
    
    Dim strA As String
    Dim strLine As String
    
    'strA = strSisteLinje & vbCrLf
    Do Until EOF(intFileNr)
        Line Input #intFileNr, strLine
        strA = strA & strLine & vbCrLf
    Loop
    
    strA = Replace(strA, "{inf}", "<infolinje>", , 1)
    strA = Replace(strA, "{ql}{cl}", "</infolinje>", , 1)
    
    strA = Replace(strA, "{te}{lbr}", "<oversikt type=""", , 1)
    strA = Replace(strA, "{rbr}{cl}" & vbCrLf, """>", , 1)
    strA = Replace(strA, "{ql}{cl}", "</oversikt>", , 1)

    strA = Replace(strA, "{ql}{cl}", "<br/>")
    
    strA = Replace(strA, "{lbr}", "<p type=""", , 1)
    strA = Replace(strA, "{rbr}{cl}", """>")
    strA = Replace(strA, "{lbr}", "</p>" & vbCrLf & "<p type=""")
    
    'strA = Replace(strA, "{rbr}{cl}", """>" & vbCrLf & "<table>")
    'strA = Replace(strA, "{lbr}", "</table>" & vbCrLf & "</p>" & vbCrLf & "<p type=""")
    
    strA = Replace(strA, "{tr}{cl}", "</td></tr>")
    strA = Replace(strA, "{tr}", "</td><td>")
    strA = Replace(strA, "{stl}", "<tr><td>")
    strA = Replace(strA, "{spl1}{tl}", "</td><td>")
    
'*** Fjerner extra CrLf:
    strA = Replace(strA, "<p type=""se1"">" & vbCrLf, "<p type=""se1"">")
    strA = Replace(strA, "<p type=""se5"">" & vbCrLf, "<p type=""se5"">")
    
    strA = Replace(strA, "<p type=""ov0"">" & vbCrLf, "<p type=""ov0"">")
    strA = Replace(strA, "<p type=""ov00"">" & vbCrLf, "<p type=""ov00"">")
    
    strA = Replace(strA, "<br/>" & vbCrLf & "</p>", "</p>")

'*** Ekstra for � rette bugs i kildefila
    'strA = Replace(strA, "{spl1} {tl}", "</td><td>")
    'strA = Replace(strA, "{spl1}", "</td><td>")

    'GetMainPart = "<xmltext>" & vbCrLf & strA & "</table>" & vbCrLf & "</p>" & vbCrLf & "</xmltext>" & vbCrLf
    GetMainPart = "<xmltext>" & vbCrLf & strA & "</p>" & vbCrLf & "</xmltext>" & vbCrLf
End Function

Private Function GetFipHeaderInfo(strA As String) As String

End Function

Private Function MakeAdmInfo(strFipHeader As String, strMainPart As String, strFileDate As String, strFilnavn As String, intJobNr As Integer) As String
    Dim strTemp As String
    Dim xmlDoc As DOMDocument
    Dim strStikkord As String
    Dim strOversiktType As String
    Dim strSubgroup As String
    Dim strFylke As String
    Dim strKommune As String
    Dim strBy As String
    Dim strKrets As String
    Dim strPrioritet As String
    Dim strKommuneNr As String
    Dim strKretsNr As String
    Dim strType As String
    
    '*** Hent verdier fra FIP-header
    strSubgroup = HentFipTag(strFipHeader, "BU")
    strPrioritet = HentFipTag(strFipHeader, "PR")
    
    strFylke = HentFipTag(strFipHeader, "LF")
    strKommune = HentFipTag(strFipHeader, "LK")
    strBy = HentFipTag(strFipHeader, "LB")
    strKrets = HentFipTag(strFipHeader, "LE")
    strFileDate = HentFipTag(strFipHeader, "VD")
    strFileDate = strFileDate & " " & HentFipTag(strFipHeader, "VT")
    
    '*** Hent verdier fra XML-delen av fila
    Set xmlDoc = New DOMDocument
    If Not LoadXmlDoc(xmlDoc, strMainPart, False, strMessage) Then
        'MsgBox strMessage
        SkrivErr "Valg: MakeAdmInfo", strMessage, 0, strFilnavn
        Exit Function
    End If
    
    strStikkord = xmlDoc.selectSingleNode("/*/oversikt").Text
    strOversiktType = xmlDoc.selectSingleNode("/*/oversikt/@type").Text
                        
    '*** Hent verdier ut i fra filnavnet
    TolkFilnavn strFilnavn, strType, strKommuneNr, strKretsNr
                        
    strTemp = strTemp & "<valget>" & vbCrLf
    strTemp = strTemp & "   <type>" & strSubgroup & "</type>" & vbCrLf
    strTemp = strTemp & "   <prioritet>" & strPrioritet & "</prioritet>" & vbCrLf
    strTemp = strTemp & "   <filtype>" & strType & "</filtype>" & vbCrLf
    
    If strFylke <> "" Then
         strTemp = strTemp & "   <fylke>" & strFylke & "</fylke>" & vbCrLf
    End If
    If strKommune <> "" Then
         strTemp = strTemp & "   <kommune>" & strKommune & "</kommune>" & vbCrLf
    End If
    If strBy <> "" Then
         strTemp = strTemp & "   <by>" & strBy & "</by>" & vbCrLf
    End If
    If strKrets <> "" Then
         strTemp = strTemp & "   <krets>" & strKrets & "</krets>" & vbCrLf
    End If
    If strKommuneNr <> "" Then
         strTemp = strTemp & "   <kommunenr>" & strKommuneNr & "</kommunenr>" & vbCrLf
    End If
    If strKretsNr <> "" Then
         strTemp = strTemp & "   <kretsnr>" & strKretsNr & "</kretsnr>" & vbCrLf
    End If
    
    strTemp = strTemp & "</valget>" & vbCrLf
    
    strTemp = strTemp & "<adm>" & vbCrLf
    strTemp = strTemp & "<timestamp>" & Format(strFileDate, "yyyymmdd hh:mm:ss") & "</timestamp>" & vbCrLf '20010813 10.55.03
    strTemp = strTemp & "<timenitf>" & Format(strFileDate, "yyyymmddThhmmss") & "</timenitf>" & vbCrLf     '20010813T105503
    strTemp = strTemp & "<date.issue>" & Format(strFileDate, "yyyymmdd") & "</date.issue>" & vbCrLf          '200108
    strTemp = strTemp & "<visdato>" & Format(strFileDate, "dd.mm.yyyy hh:mm") & "</visdato>" & vbCrLf      '13.08.2001 10:55
    strTemp = strTemp & "<folder>Valg</folder>" & vbCrLf
    strTemp = strTemp & LagXmlTag("filename", strFilnavn)
    strTemp = strTemp & LagXmlTag("fname", GetFileBase(strFilnavn, False))
    strTemp = strTemp & "<madebydistributor>VLG 2001</madebydistributor>" & vbCrLf
    strTemp = strTemp & "</adm>" & vbCrLf

    strTemp = strTemp & "<fields>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBMeldingsSign</name><value>hlh-kbj</value></field>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBPrioritet</name><value>" & strPrioritet & "</value></field>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBStikkord</name><value>" & strStikkord & "</value></field>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBOmraader</name><value>Norge;</value></field>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBDistribusjonsKode</name><value>ALL</value></field>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBKanal</name><value>A</value></field>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBSendTilDirekte</name><value>False</value></field>" & vbCrLf
    strTemp = strTemp & "<field><name>NTBMeldingsType</name><value>Standard nyhetsmelding</value></field>" & vbCrLf
    strTemp = strTemp & "</fields>" & vbCrLf
    
    strTemp = strTemp & "<group>VLG</group>" & vbCrLf
    strTemp = strTemp & "<subgroup>" & strSubgroup & "</subgroup>" & vbCrLf
    strTemp = strTemp & "<category>Politikk;</category>" & vbCrLf
    strTemp = strTemp & "<subject>" & strStikkord & "</subject>" & vbCrLf
    strTemp = strTemp & "<subcategory></subcategory>" & vbCrLf

    MakeAdmInfo = strTemp
    
    Set xmlDoc = Nothing
   

End Function

Private Function HentFipTag(strFipHeader As String, strFipTag As String) As String
    Dim lngStart As Long
    Dim lngSlutt As Long
    Dim strTemp As String
            
    strFipTag = Chr(10) & strFipTag & ":"
    lngStart = InStr(1, strFipHeader, strFipTag)
    If lngStart = 0 Then
        HentFipTag = ""
        Exit Function
    End If
    lngStart = lngStart + Len(strFipTag)
    lngSlutt = InStr(lngStart, strFipHeader, Chr(10))
    
    strTemp = Mid(strFipHeader, lngStart, lngSlutt - lngStart - 1)
    strTemp = Replace(strTemp, Chr(13), "")
    HentFipTag = strTemp
End Function

Private Sub TolkFilnavn(strFilnavn As String, strType As String, strKommuneNr As String, strKretsNr As String)
    Dim strTemp As String
    Dim strTypeNr As String
    Dim intPos As Integer
    
    strTypeNr = Mid(strFilnavn, 4, 2)
    intPos = InStr(1, strFilnavn, ".")
    strTemp = ""
    If intPos > 6 Then
        strTemp = Mid(strFilnavn, 7, intPos - 7)
    Else
    End If
    
    strKommuneNr = ""
    strKretsNr = ""
    
    Select Case strTypeNr
    Case "01"
        strType = "Kretsresultat"
        strKommuneNr = Mid(strTemp, 1, 2) & Mid(strTemp, 4, 2)
        strKretsNr = Mid(strTemp, 7)
    Case "02"
        strType = "Kommuneresultat"
        strKommuneNr = Mid(strTemp, 1, 2) & Mid(strTemp, 4, 2)
    Case "03"
        strType = "Fylkesoversikt"
        strKommuneNr = Mid(strTemp, 1, 2) & "00"
    Case "04"
        strType = "Landoversikt"
    Case "07"
        strType = "Byoversikt"
        strKommuneNr = Mid(strTemp, 1, 2) & Mid(strTemp, 4, 2)
    End Select
End Sub

