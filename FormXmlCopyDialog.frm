VERSION 5.00
Begin VB.Form FormXmlCopyDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "XML Copy"
   ClientHeight    =   3270
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   8190
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   8190
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtHit 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1044
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   3600
      TabIndex        =   3
      Text            =   "Text1"
      Top             =   1800
      Width           =   615
   End
   Begin VB.ComboBox ComboCompare 
      Height          =   315
      ItemData        =   "FormXmlCopyDialog.frx":0000
      Left            =   1320
      List            =   "FormXmlCopyDialog.frx":0010
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   1800
      Width           =   1455
   End
   Begin VB.ComboBox ComboType 
      Height          =   315
      ItemData        =   "FormXmlCopyDialog.frx":0024
      Left            =   1320
      List            =   "FormXmlCopyDialog.frx":002E
      TabIndex        =   6
      Text            =   "Combo1"
      Top             =   120
      Width           =   1695
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   5400
      TabIndex        =   4
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   6840
      TabIndex        =   5
      Top             =   2760
      Width           =   1215
   End
   Begin VB.TextBox txtPath 
      Height          =   375
      Left            =   1320
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   600
      Width           =   6735
   End
   Begin VB.TextBox txtXpath 
      Height          =   375
      Left            =   1320
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   1320
      Width           =   6735
   End
   Begin VB.Label Label5 
      Caption         =   "Treff:"
      Height          =   255
      Left            =   3000
      TabIndex        =   11
      Top             =   1800
      Width           =   495
   End
   Begin VB.Label Label4 
      Caption         =   "Test:"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   1800
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Type:"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "XPath s�k:"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1440
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Ut-Path:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   720
      Width           =   975
   End
End
Attribute VB_Name = "FormXmlCopyDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim recXmlCopy As typXmlCopy
Dim recXmlCopyTom As typXmlCopy
Public lpCopyIndex As Long
Public intRow As Integer
Const INT_BLANK = 183
Dim strBlank As String

Private Sub cmdOK_Click()
    '*** sett inn kode her
    With recXmlCopy
        .strType = Me.ComboType
        .strPath = Me.txtPath
        .strFind = Me.txtXpath
        .intHit = Me.txtHit
        .strCompare = Me.ComboCompare
    End With
    If lpCopyIndex = 0 Then
        LagNy
    End If
    
    arrXmlCopy(lpCopyIndex) = recXmlCopy
    FormXmlCopy.VisEnRad intRow, lpCopyIndex
    Unload Me
End Sub

Private Sub Form_Load()
    With recXmlCopy
        If lpCopyIndex > 0 Then
            recXmlCopy = arrXmlCopy(lpCopyIndex)
        Else
            recXmlCopy = recXmlCopyTom
            .strType = "xpath"
            .strCompare = "gt"
        End If
        
        Me.ComboType = .strType
        Me.txtPath = .strPath
        Me.txtXpath = .strFind
        Me.txtHit = .intHit
        Me.ComboCompare = .strCompare
    End With
End Sub

Private Sub cmdAvbryt_Click()
    Unload Me
End Sub

Private Sub txtFind_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 32 Then
'        KeyAscii = INT_BLANK
'    End If
End Sub

Private Sub txtReplace_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 32 Then
'        KeyAscii = INT_BLANK
'    End If
End Sub

Public Sub LagNy()
    On Error GoTo Err_Handler
    Dim lpIndex As Long
    Dim intRow As Integer
    Dim lpPrevNode As Long
    
    lpPrevNode = arrTrans(intJobIndex).lpCopyEnd
    lpIndex = UBound(arrXmlCopy) + 1

    arrXmlCopy(lpPrevNode).lpNexNode = lpIndex
    
    ReDim Preserve arrXmlCopy(lpIndex)
    arrXmlCopy(lpIndex) = recXmlCopy
    arrTrans(intJobIndex).lpCopyEnd = lpIndex
    
    Exit Sub
Err_Handler:
    MsgBox Err.Description, vbCritical, "Feil"
End Sub


