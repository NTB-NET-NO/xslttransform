Attribute VB_Name = "XmlCopy"
Option Explicit

Public arrXmlCopy() As typXmlCopy

Type typXmlCopy
    intJobNr    As Integer
    strType     As String
    strFind     As String
    strCompare  As String
    intHit      As Integer
    strPath     As String
    lpNexNode   As Long
'    lpPrevNode  As Long
End Type

Public Function InitXmlCopy(intJobNr As Integer) As Boolean
    Dim strFilnavn As String
    Dim xmlDoc As DOMDocument
    
    With arrTrans(intJobNr)
        If LCase(Right(.strCopyFile, 4) <> ".xml") Then
            InitXmlCopy = True
            .lpCopyStart = 0
            Exit Function
        End If
        strFilnavn = recGeneral.strRootPath & "\copy\" & .strCopyFile
        If Not LoadXmlDoc(xmlDoc, strFilnavn, True, strMessage, True) Then
            .lpCopyStart = 0
            SkrivErr "InitXmlCopy", strMessage, intJobNr, strFilnavn
            InitXmlCopy = False
            Exit Function
        End If
        
        FyllXmlCopy xmlDoc, "/copyxml/*", intJobNr, .lpCopyStart, .lpCopyEnd
        
    End With
    
    InitXmlCopy = True
    Exit Function
Err_Handler:
    InitXmlCopy = False
    strMessage = Err.Description
End Function

Private Sub FyllXmlCopy(xmlDoc As DOMDocument, strXpath As String, intJobNr As Integer, lpStart As Long, lpEnd As Long)
    On Error GoTo Err_Handler
    Dim lpIndex As Long
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlNode As IXMLDOMNode
    
    lpIndex = UBound(arrXmlCopy)
    lpStart = lpIndex + 1
    Dim i As Integer
    Set xmlNodeList = xmlDoc.selectNodes(strXpath)
    For i = 0 To xmlNodeList.length - 1
        Set xmlNode = xmlNodeList(i)
        lpIndex = lpIndex + 1
        ReDim Preserve arrXmlCopy(lpIndex)
        With arrXmlCopy(lpIndex)
            .intJobNr = intJobNr
            .strType = xmlNode.nodeName
            Select Case .strType
            Case "file"
                .strPath = xmlNode.Attributes.Item(0).Text
            Case "xpath"
                .strPath = xmlNode.Attributes.Item(0).Text
                .strCompare = xmlNode.Attributes.Item(1).Text
                .intHit = xmlNode.Attributes.Item(2).Text
                .strFind = xmlNode.childNodes.Item(0).Text
            Case Else
            End Select
            .lpNexNode = lpIndex + 1
            If i = 0 Then
                '***.lpPrevNode = 0
            Else
                '***.lpPrevNode = lpIndex - 1
            End If

        End With
    Next
    arrXmlCopy(lpIndex).lpNexNode = 0
    lpEnd = lpIndex
    Exit Sub
Err_Handler:
    SkrivErr "FyllXmlCopy", Err.Description, intJobNr
    arrTrans(intJobNr).strErrStatus = STR_FEIL
End Sub

Public Function xmlCopyFiles(strFileName As String, strOutFile As String, intJobNr As Integer, GlobLokal As typGlob) As Boolean
    On Error GoTo Err_Handler
    'Dim xmlDoc As DOMDocument
    Dim strOutPath As String
    Dim bLoaded As Boolean
    
    Dim lpStart As Long
    Dim lpIndex As Long
    
    lpStart = arrTrans(intJobNr).lpCopyStart
    
    If GlobLokal.Type = typXmlDoc Then
        GlobLokal.strText = arrTrans(intJobNr).xmlDoc.xml
    End If
    
    lpIndex = lpStart
    Do
        With arrXmlCopy(lpIndex)
            
            If .strPath = "" Then
                strOutPath = arrTrans(intJobNr).strOutPath & "\"
            Else
                strOutPath = .strPath & "\"
            End If
            
            Select Case .strType
            
            Case "file"
                If Not GlobLokal.bLoaded Then
                    FSO.CopyFile strFileName, strOutPath & strOutFile
                    SkrivLog "JobNr:" & Format(Str(intJobNr), "00") & " copy", strFileName, strOutPath
                Else
                    If SaveFile(strOutPath & strOutFile, Glob.strText, intJobNr) Then
                        SkrivLog "JobNr:" & Format(Str(intJobNr), "00") & " copy", strFileName, strOutPath
                    End If
                End If

            Case "xpath"
'                If Not bLoaded Then
'                    If Not LoadXmlDoc(xmlDoc, strFileName, True, strMessage, True) Then
'                        SkrivErr "xmlCopyFiles", strMessage, intJobNr, strFileName
'                        Exit Function
'                    End If
'                    bLoaded = True
'                End If
                If Not LoadFile(intJobNr, strFileName, GlobLokal, typXmlDoc) Then
                    SkrivErr "xmlCopyFiles", strMessage, intJobNr, strFileName
                    Exit Function
                End If
                If FinnXmlXpath(arrTrans(intJobNr).xmlDoc, .strFind, .strCompare, .intHit, intJobNr) Then
                    'FSO.CopyFile strFileName, strOutPath
                    If SaveFile(strOutPath & strOutFile, Glob.strText, intJobNr) Then
                        SkrivLog "JobNr:" & Format(Str(intJobNr), "00") & " copy", strFileName, strOutPath
                    End If
                End If
            End Select
            lpIndex = .lpNexNode
        End With
        
    Loop Until lpIndex = 0
    
    'Set xmlDoc = Nothing
    xmlCopyFiles = True
    Exit Function
    
Err_Handler:
    xmlCopyFiles = False
End Function

Private Function FinnXmlXpath(xmlDoc As DOMDocument, strFind As String, strCompare As String, intHit As Integer, Optional intJobNr As Integer) As Boolean
    On Error GoTo Err_Handler
    Dim xmlNodeList As IXMLDOMNodeList
    Dim lpCount As Long

    Set xmlNodeList = xmlDoc.selectNodes(strFind)
    lpCount = xmlNodeList.length
    
    Select Case strCompare
    Case "gt"
        FinnXmlXpath = lpCount > intHit
    Case "eq"
        FinnXmlXpath = lpCount = intHit
    Case "lt"
        FinnXmlXpath = lpCount < intHit
    Case "ne"
        FinnXmlXpath = lpCount <> intHit
    Case Else
        FinnXmlXpath = False
    End Select
    
    Exit Function
Err_Handler:
    FinnXmlXpath = False
    SkrivErr "FinnXmlXpath", Err.Description, intJobNr
    
End Function

