VERSION 5.00
Begin VB.Form FormReplaceDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dialog Caption"
   ClientHeight    =   7530
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   11265
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   11265
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox ComboCompare 
      Height          =   315
      ItemData        =   "FormReplaceDialog.frx":0000
      Left            =   9480
      List            =   "FormReplaceDialog.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   2640
      Width           =   1695
   End
   Begin VB.TextBox txtCount 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1044
         SubFormatType   =   1
      EndProperty
      Height          =   375
      Left            =   10440
      TabIndex        =   11
      Text            =   "Text1"
      Top             =   2040
      Width           =   735
   End
   Begin VB.TextBox txtStart 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1044
         SubFormatType   =   1
      EndProperty
      Height          =   375
      Left            =   10440
      TabIndex        =   10
      Text            =   "Text1"
      Top             =   1440
      Width           =   735
   End
   Begin VB.CheckBox CheckVisBlanke 
      Caption         =   "&Vis blanke"
      Height          =   435
      Left            =   9960
      TabIndex        =   13
      Top             =   3240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Frame FrameFind2 
      Caption         =   "S�k etter neste"
      Height          =   1335
      Left            =   120
      TabIndex        =   17
      Top             =   1560
      Width           =   8895
      Begin VB.CheckBox CheckFind2Ent 
         Caption         =   "Ascii som XML"
         Height          =   435
         Left            =   7320
         TabIndex        =   3
         Top             =   120
         Width           =   1455
      End
      Begin VB.TextBox txtFind2 
         Height          =   975
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   2
         Text            =   "FormReplaceDialog.frx":001C
         Top             =   240
         Width           =   7095
      End
   End
   Begin VB.Frame FrameReplace2 
      Caption         =   "Erstatt neste med"
      Height          =   1575
      Left            =   120
      TabIndex        =   16
      Top             =   5760
      Width           =   8895
      Begin VB.CheckBox CheckReplace2Ent 
         Caption         =   "Ascii som XML"
         Height          =   195
         Left            =   7320
         TabIndex        =   7
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox txtReplace2 
         Height          =   1215
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   6
         Text            =   "FormReplaceDialog.frx":0022
         Top             =   240
         Width           =   7095
      End
   End
   Begin VB.Frame FrameReplace 
      Caption         =   "Erstatt med"
      Height          =   2655
      Left            =   120
      TabIndex        =   15
      Top             =   3000
      Width           =   8895
      Begin VB.TextBox txtReplace 
         Height          =   2175
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   4
         Text            =   "FormReplaceDialog.frx":0028
         Top             =   240
         Width           =   7095
      End
      Begin VB.CheckBox CheckReplaceEnt 
         Caption         =   "Ascii som XML"
         Height          =   195
         Left            =   7320
         TabIndex        =   5
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.Frame FrameFind 
      Caption         =   "S�k etter"
      Height          =   1335
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   8895
      Begin VB.TextBox txtFind 
         Height          =   975
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   0
         Text            =   "FormReplaceDialog.frx":002E
         Top             =   240
         Width           =   7095
      End
      Begin VB.CheckBox CheckFindEnt 
         Caption         =   "Ascii som XML"
         Height          =   435
         Left            =   7320
         TabIndex        =   1
         Top             =   120
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   9960
      TabIndex        =   9
      Top             =   840
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   9960
      TabIndex        =   8
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Antall:"
      Height          =   255
      Left            =   9480
      TabIndex        =   19
      Top             =   2040
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Start pos.:"
      Height          =   255
      Left            =   9480
      TabIndex        =   18
      Top             =   1440
      Width           =   855
   End
End
Attribute VB_Name = "FormReplaceDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim recXchgReplace As typXchgReplace
Dim recXchgReplaceTom As typXchgReplace
Public lpXpathIndex As Long
Public lpXchgIndex As Long
Const INT_BLANK = 183
Dim strBlank As String
Public strType As String
Public intRow As Integer

Private Sub Form_Load()
    If lpXpathIndex = 0 Then
        recXchgReplace = recXchgReplaceTom
        recXchgReplace.intCompare = 0
        recXchgReplace.intStart = 1
        recXchgReplace.intCount = -1
    Else
        recXchgReplace = arrXchgReplace(lpXpathIndex)
    End If
    With recXchgReplace
        Select Case strType
        Case "pair"
            Me.FrameFind2.Visible = True
            Me.FrameReplace2.Visible = True
            Me.FrameReplace2.Caption = "Erstatt neste med"
            Me.ComboCompare.Visible = False
        Case "bulk"
            Me.FrameFind2.Visible = True
            Me.FrameReplace2.Visible = False
            Me.ComboCompare.Visible = False
            Me.Height = 6330
        Case Else
            Me.FrameFind2.Visible = False
            Me.FrameReplace2.Visible = False
            Me.FrameReplace.Top = 1560
            Me.ComboCompare.Visible = True
            Me.Height = 4995
        End Select
        Me.txtStart = .intStart
        Me.txtCount = .intCount
        
        Select Case .intCompare
        Case 1
            Me.ComboCompare.Text = "Text"
        Case Else
            Me.ComboCompare.Text = "Binary"
        End Select
        
        Me.txtFind = Replace(.strFindShow, " ", Chr(INT_BLANK))
        Me.CheckFindEnt = .bFindEnt * -1
        Me.txtReplace = Replace(.strReplaceShow, " ", Chr(INT_BLANK))
        Me.CheckReplaceEnt = .bReplaceEnt * -1
        
        Me.txtFind2 = Replace(.strFind2Show, " ", Chr(INT_BLANK))
        Me.CheckFind2Ent = .bFind2Ent * -1
        
        Me.txtReplace2 = Replace(.strReplace2Show, " ", Chr(INT_BLANK))
        Me.CheckReplace2Ent = .bReplace2Ent * -1
    End With
End Sub

Private Sub cmdOK_Click()
    '*** sett inn kode her
    With recXchgReplace
        
        .strFindShow = Replace(Me.txtFind, Chr(INT_BLANK), " ")
        .bFindEnt = Me.CheckFindEnt
        .strReplaceShow = Replace(Me.txtReplace, Chr(INT_BLANK), " ")
        .bReplaceEnt = Me.CheckReplaceEnt
        
        If .bFindEnt Then
            .strFind = .strFindShow
        Else
            .strFind = LagCharStreng(.strFindShow)
        End If
        If .bReplaceEnt Then
            .strReplace = .strReplaceShow
        Else
            .strReplace = LagCharStreng(.strReplaceShow)
        End If
    
        .strFind2Show = Replace(Me.txtFind2, Chr(INT_BLANK), " ")
        .bFind2Ent = Me.CheckFind2Ent
        .strReplace2Show = Replace(Me.txtReplace2, Chr(INT_BLANK), " ")
        .bReplace2Ent = Me.CheckReplace2Ent
        
        If .bFind2Ent Then
            .strFind2 = .strFind2Show
        Else
            .strFind2 = LagCharStreng(.strFind2Show)
        End If
        If .bReplaceEnt Then
            .strReplace2 = .strReplace2Show
        Else
            .strReplace2 = LagCharStreng(.strReplace2Show)
        End If
    
        Select Case Me.ComboCompare.Text
        Case "Text"
            .intCompare = 1
        Case Else
            .intCompare = 0
        End Select
        .intStart = Me.txtStart
        .intCount = Me.txtCount
    
    End With
    
    If lpXpathIndex = 0 Then
        LagNy lpXpathIndex
        intRow = intRow + 1
    End If
    
    arrXchgReplace(lpXpathIndex) = recXchgReplace
    
    FormReplace.VisEnRad intRow, lpXpathIndex
    Unload Me
End Sub

Private Sub cmdAvbryt_Click()
    Unload Me
End Sub

Private Sub txtFind_KeyPress(KeyAscii As Integer)
    VisBlanke KeyAscii
End Sub

Private Sub VisBlanke(KeyAscii As Integer)
    If KeyAscii = 32 Then
        KeyAscii = INT_BLANK
    End If
End Sub

Private Sub txtFind2_KeyPress(KeyAscii As Integer)
    VisBlanke KeyAscii
End Sub

Private Sub txtReplace_KeyPress(KeyAscii As Integer)
    VisBlanke KeyAscii
End Sub

Private Sub txtReplace2_KeyPress(KeyAscii As Integer)
    VisBlanke KeyAscii
End Sub

Private Sub LagNy(lpIndex As Long)
    On Error GoTo Err_Handler
    'Dim lpIndex As Long
    Dim intRow As Integer
    Dim lpPrevNode As Long
        
    lpPrevNode = arrXmlXchg(lpXchgIndex).lpReplaceEnd
    lpIndex = UBound(arrXchgReplace) + 1
    
    arrXchgReplace(lpPrevNode).lpNexNode = lpIndex
    
    ReDim Preserve arrXchgReplace(lpIndex)
    arrXmlXchg(lpXchgIndex).lpReplaceEnd = lpIndex

    Exit Sub
Err_Handler:
    MsgBox Err.Description, vbCritical, "Feil"
End Sub


