Attribute VB_Name = "XmlDom"
Option Explicit

Public Function LoadXmlDoc(xmlDoc As DOMDocument, strXml As String, bAsFile As Boolean, strMessage As String, Optional bNew As Boolean) As Boolean
    '*** Henter XML_fil eller tekst inn som DOM-trestruktur:
    On Error GoTo Err_Handler
    Dim xmlError As IXMLDOMParseError
    
    If bNew Then
        Set xmlDoc = New DOMDocument
    End If
    
    If bAsFile Then
        '*** XML fil
        xmlDoc.async = False
        xmlDoc.Load strXml
    Else
        '*** XML tekst
        xmlDoc.loadXML strXml
    End If

    Set xmlError = xmlDoc.parseError

    If (xmlError.errorCode <> 0) Then
        strMessage = "Feil: " & xmlError.reason & _
             "I linje: " & xmlError.Line
        strMessage = Replace(strMessage, vbCrLf, " ")
        LoadXmlDoc = False
    Else
        LoadXmlDoc = True
    End If
Exit Function
Err_Handler:
    LoadXmlDoc = False
    strMessage = Err.Description
End Function

Public Function HentXmlNodeText(xmlJobNode As IXMLDOMNode, strXpath As String, strDefault As String) As String
    On Error GoTo Err_Handler
    Dim strString As String
    Dim xmlNode As IXMLDOMNode
    
    Set xmlNode = xmlJobNode.selectSingleNode(strXpath)
    If xmlNode Is Nothing Then
        HentXmlNodeText = "" 'strDefault
    Else
        If xmlNode.Text <> "" Then
            HentXmlNodeText = xmlNode.Text
        Else
            HentXmlNodeText = strDefault
        End If
    End If
    Exit Function
Err_Handler:
    HentXmlNodeText = ""
    
End Function

'Public Function LagXmlTag(strName As String, strText As String) As String
'    If Trim(strText) <> "" Then
'        LagXmlTag = "<" & strName & ">" & strText & "</" & strName & ">" & vbCrLf
'    End If
'End Function
'
Public Function LagXmlTag(strName As String, strText As String, Optional strAttributes As String, Optional intTagType As Integer, Optional bCData As Boolean, Optional intTab As Integer) As String
    Dim arrAttr As Variant
    Dim arrAttrVal As Variant
    Dim strAttr As String
    Dim i As Integer
    Dim strTab As String
    
    strTab = String(intTab, Chr(9))
    
    If Trim(strName) <> "" Then
        If strAttributes <> "" Then
            arrAttr = Split(strAttributes, ";")
            For i = 0 To UBound(arrAttr)
                arrAttrVal = Split(arrAttr(i), "=")
                strAttr = strAttr & " " & arrAttrVal(0) & "=""" & arrAttrVal(1) & """"
            Next
        End If
        Select Case intTagType
        Case 0
            'If strText = "" Then
            '    LagXmlTag = strTab & "<" & strName & strAttr & "/>" & vbCrLf
            'Else
                If bCData Then
                    LagXmlTag = strTab & "<" & strName & strAttr & "><![CDATA[" & strText & "]]></" & strName & ">" & vbCrLf
                Else
                    LagXmlTag = strTab & "<" & strName & strAttr & ">" & strText & "</" & strName & ">" & vbCrLf
                End If
            'End If
        Case 1
            LagXmlTag = strTab & "<" & strName & strAttr & ">" & strText & vbCrLf
        Case 2
            LagXmlTag = strTab & "</" & strName & ">" & vbCrLf
        End Select
    End If
End Function

Public Sub TestXmlDom()
    '*** Testfunksjon
    Dim xmlDoc As DOMDocument
    Dim xmlNodeList As IXMLDOMNodeList
    Dim xmlNode As IXMLDOMNode
    Dim xmlElement As IXMLDOMElement
    Dim xmlDocFrag As IXMLDOMDocumentFragment
    
    Dim strTest As String
    Dim i As Integer
    Dim n As Integer
    Dim strRefnum As String
    Dim strCode As String
    Dim strType As String
       
    strTest = InputBox("Oppgi tekst: ")
       
    Set xmlDoc = New DOMDocument
    xmlDoc.async = False
    If Not LoadXmlDoc(xmlDoc, strTest, False, strMessage) Then
        MsgBox strMessage
        Exit Sub
    End If
               
    xmlDoc.save "C:\TestXml.xml"
    
Exit Sub
    
    strTest = "//docdata"
    strTest = InputBox("Oppgi XPATH: ", "", strTest)
    'MsgBox xmlDoc.selectNodes(strTest).Item(0).xml
    TestNodeList xmlDoc.selectNodes(strTest), True
    
    xmlDoc.save "C:\TestNodelist.xml"
    
    '*** Henter xml-elementer (tag-innhold) eller attributter fra entydige noder:
    strTest = xmlDoc.selectSingleNode("/nitf/head/tobject/@tobject.type").Text
    strTest = xmlDoc.selectSingleNode("/nitf/head/tobject/tobject.property/@tobject.property.type").Text
    
    Set xmlNode = xmlDoc.selectSingleNode("/nitf/head/tobject")
    
    Set xmlDocFrag = xmlDoc.selectSingleNode("/nitf/head/tobject").cloneNode(True)
    
    strTest = xmlNode.xml
    xmlNode = strTest
    
    '*** Henter liste over alle "tobject.subject" noder
    Set xmlNodeList = xmlDoc.selectNodes("/nitf/head/tobject/tobject.subject")
    If Not xmlNodeList Is Nothing Then
        For i = 0 To xmlNodeList.length - 1
            '*** Lister attributtene til "tobject.subject" ut fra kjent rekkef�lge
            strRefnum = xmlNodeList.Item(i).Attributes.Item(0).Text
            strCode = xmlNodeList.Item(i).Attributes.Item(1).Text
            strType = xmlNodeList.Item(i).Attributes.Item(2).Text
        Next

        '*** Alternativ m�te:
        For i = 0 To xmlNodeList.length - 1
            '*** Lister attributtene til "tobject.subject" etter rekkef�lge
            For n = 0 To xmlNodeList.Item(i).Attributes.length - 1
                strTest = xmlNodeList.Item(i).Attributes.Item(n).nodeName
                strTest = xmlNodeList.Item(i).Attributes.Item(n).Text
            Next
        Next

    End If

    Set xmlDoc = Nothing
End Sub

Public Function TestNodeList(xmlNodeList As IXMLDOMNodeList, Optional bCData As Boolean, Optional intLevel As Integer) As Boolean
    Dim strText As String
    Dim i As Long
    Dim n As Long
    
    If Not xmlNodeList Is Nothing Then
        For i = 0 To xmlNodeList.length - 1
            strText = xmlNodeList.Item(i).nodeTypeString
            Select Case xmlNodeList.Item(i).nodeType
            Case 2
                xmlNodeList.Item(i).Text = "*-*" & xmlNodeList.Item(i).Text
            Case 3, bCData * -4
                xmlNodeList.Item(i).Text = "***" & xmlNodeList.Item(i).Text
                strText = xmlNodeList.Item(i).xml
            Case 1
                strText = xmlNodeList.Item(i).Text
                If intLevel < 1 Then
                    TestNodeList xmlNodeList.Item(i).childNodes, bCData, 1
                End If
            End Select
        Next
    End If
End Function

Public Function XmlXchg(xmlDoc As DOMDocument, intJobNr As Integer) As Boolean
    On Error GoTo Err_Handler
    Dim xmlNodeList As IXMLDOMNodeList
    Dim i As Integer
    Dim strXpath As String
    Dim strXml As String
    Dim xmlXpathNode As IXMLDOMNode
    
    With arrTrans(intJobNr)
        If .strXchgFile = "" Then Exit Function
        
'        Set xmlNodeList = .xmlXchg.selectNodes("/xchg/xpath")
        For i = 0 To xmlNodeList.length - 1
            Set xmlXpathNode = xmlNodeList.Item(i)
            strXpath = xmlXpathNode.Attributes.getNamedItem("string").Text
            If strXpath <> "" Then
                xmlSearchTree xmlDoc.selectNodes(strXpath), xmlXpathNode, True
            Else
'                strXml = xmlReplace(xmlDoc.xml, xmlXpathNode)
'                If Not LoadXmlDoc(xmlDoc, strXml, False, strMessage) Then
'                    MsgBox strMessage
'                End If
            End If

        Next
    End With
    XmlXchg = True
    Exit Function
Err_Handler:
    XmlXchg = False
End Function

Public Function xmlSearchTree(xmlKildeNodeList As IXMLDOMNodeList, xmlXpathNode As IXMLDOMNode, Optional bCData As Boolean, Optional intLevel As Integer) As Boolean
    Dim strText As String
    Dim i As Long
    Dim n As Long

    For i = 0 To xmlKildeNodeList.length - 1
        Select Case xmlKildeNodeList.Item(i).nodeType
        Case 2
            xmlKildeNodeList.Item(i).Text = xmlReplace(xmlKildeNodeList.Item(i).Text, xmlXpathNode)
        Case 3, bCData * -4
            xmlKildeNodeList.Item(i).Text = xmlReplace(xmlKildeNodeList.Item(i).Text, xmlXpathNode)
        Case 1
            If intLevel < 1 Then
                xmlSearchTree xmlKildeNodeList.Item(i).childNodes, xmlXpathNode, bCData, 1
            End If
        End Select
    Next
End Function
   
Public Function xmlReplace(strXmlKilde As String, xmlXpathNode As IXMLDOMNode) As String
    Dim xmlNodeList As IXMLDOMNodeList
    Dim strFrom As String
    Dim strReplace As String
    Dim strXpath As String
    Dim intStart As Integer
    Dim intCount As Integer
    Dim intCompare As Integer

    Dim i As Integer
   
    Set xmlNodeList = xmlXpathNode.selectNodes("replace")
    
    For i = 0 To xmlNodeList.length - 1
        'strFrom = xmlNodeList.Item(i).selectSingleNode("f").Text
        'strReplace = xmlNodeList.Item(i).selectSingleNode("r").Text
        
        'intStart = xmlNodeList.Item(i).Attributes.getNamedItem("start").Text
        'intCount = xmlNodeList.Item(i).Attributes.getNamedItem("count").Text
        'intCompare = xmlNodeList.Item(i).Attributes.getNamedItem("compare").Text
        
        strFrom = xmlNodeList.Item(i).childNodes.Item(0).Text
        strReplace = xmlNodeList.Item(i).childNodes.Item(1).Text
        
        intStart = xmlNodeList.Item(i).Attributes.Item(0).Text
        intCount = xmlNodeList.Item(i).Attributes.Item(1).Text
        intCompare = xmlNodeList.Item(i).Attributes.Item(2).Text
                
        strXmlKilde = Replace(strXmlKilde, strFrom, strReplace, intStart, intCount, intCompare)
    Next

    xmlReplace = strXmlKilde
End Function

Public Function xmlFindNodes(xmlDoc As DOMDocument, strXpath As String, intHit As Integer) As Boolean
    Dim xmlNodeList As IXMLDOMNodeList

    Set xmlNodeList = xmlDoc.selectNodes(strXpath)
    intHit = xmlNodeList.length
    
    'xmlFind = True
    Exit Function
    
Err_Handler:
    xmlFindNodes = False
End Function
   

