VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form FormEgenskaper 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Egenskaper"
   ClientHeight    =   8400
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   7755
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8400
   ScaleWidth      =   7755
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FramenPath 
      Caption         =   "&Kildemappe"
      Height          =   735
      Left            =   120
      TabIndex        =   24
      Top             =   1560
      Width           =   7455
      Begin VB.CommandButton cmdGetInPath 
         Caption         =   "..."
         Height          =   375
         Left            =   6480
         TabIndex        =   5
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtInPath 
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   6255
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Kopi attributt a&rkivert "
      Height          =   735
      Left            =   120
      TabIndex        =   29
      Top             =   6960
      Width           =   2895
      Begin VB.ComboBox comboArkiv 
         Height          =   315
         ItemData        =   "FormEgenskaper.frx":0000
         Left            =   120
         List            =   "FormEgenskaper.frx":0010
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   240
         Width           =   2535
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "&Slettestatus"
      Height          =   735
      Left            =   3240
      TabIndex        =   28
      Top             =   6960
      Width           =   4335
      Begin VB.TextBox txtDager 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3120
         TabIndex        =   19
         Top             =   240
         Width           =   975
      End
      Begin VB.ComboBox comboDelete 
         Height          =   315
         ItemData        =   "FormEgenskaper.frx":002B
         Left            =   120
         List            =   "FormEgenskaper.frx":003B
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label lblDager 
         Caption         =   "Dager:"
         Enabled         =   0   'False
         Height          =   255
         Left            =   2400
         TabIndex        =   31
         Top             =   240
         Width           =   615
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   120
      Top             =   7800
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame4 
      Caption         =   "&Jobbtype"
      Height          =   735
      Left            =   120
      TabIndex        =   23
      Top             =   720
      Width           =   7455
      Begin VB.CheckBox CheckDebug 
         Caption         =   "&Debug"
         Height          =   375
         Left            =   2760
         TabIndex        =   46
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox txtStatus 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         Locked          =   -1  'True
         TabIndex        =   32
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdVisFeil 
         Caption         =   "Siste feilmelding"
         Height          =   375
         Left            =   6000
         TabIndex        =   3
         Top             =   240
         Width           =   1335
      End
      Begin VB.CheckBox chkActivate 
         Caption         =   "&Aktiv"
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   735
      End
      Begin VB.ComboBox comboJobbtype 
         Height          =   315
         ItemData        =   "FormEgenskaper.frx":004F
         Left            =   960
         List            =   "FormEgenskaper.frx":006E
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Status:"
         Height          =   255
         Left            =   4440
         TabIndex        =   33
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Frame FrameOutPath 
      Caption         =   "&Mottakermappe"
      Height          =   735
      Left            =   120
      TabIndex        =   25
      Top             =   2400
      Width           =   7455
      Begin VB.CommandButton cmdGetOutPath 
         Caption         =   "..."
         Height          =   375
         Left            =   6480
         TabIndex        =   7
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtOutPath 
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   6255
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Jobb&navn"
      Height          =   615
      Left            =   120
      TabIndex        =   22
      Top             =   0
      Width           =   7455
      Begin VB.TextBox txtJobbnavn 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   7095
      End
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   6240
      TabIndex        =   21
      Top             =   7920
      Width           =   1335
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   4680
      TabIndex        =   20
      Top             =   7920
      Width           =   1335
   End
   Begin VB.Frame FrameTransFile 
      Caption         =   "&XSLT fil"
      Height          =   735
      Left            =   120
      TabIndex        =   26
      Top             =   4440
      Width           =   7455
      Begin VB.CommandButton cmdRedXslt 
         Caption         =   "Rediger"
         Height          =   375
         Left            =   6480
         TabIndex        =   10
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdGetTransFile 
         Caption         =   "..."
         Height          =   375
         Left            =   5640
         TabIndex        =   9
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtTransFile 
         Height          =   285
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.Frame FrameXchgFile 
      Caption         =   "&Xchg fil"
      Height          =   735
      Left            =   120
      TabIndex        =   27
      Top             =   5280
      Width           =   7455
      Begin VB.CommandButton cmdVisXchg 
         Caption         =   "Vis"
         Enabled         =   0   'False
         Height          =   375
         Left            =   5640
         TabIndex        =   34
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdRedXchg 
         Caption         =   "Rediger"
         Height          =   375
         Left            =   6480
         TabIndex        =   13
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtXchgFile 
         Height          =   285
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   4575
      End
      Begin VB.CommandButton cmdGetXchgFile 
         Caption         =   "..."
         Height          =   375
         Left            =   4800
         TabIndex        =   12
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame FrameCopyFile 
      Caption         =   "&Copy fil"
      Height          =   735
      Left            =   120
      TabIndex        =   30
      Top             =   6120
      Width           =   7455
      Begin VB.CommandButton cmdVisCopy 
         Caption         =   "Vis"
         Enabled         =   0   'False
         Height          =   375
         Left            =   5640
         TabIndex        =   35
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdRedCopy 
         Caption         =   "Rediger"
         Height          =   375
         Left            =   6480
         TabIndex        =   16
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdCopyFile 
         Caption         =   "..."
         Height          =   375
         Left            =   4800
         TabIndex        =   15
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtCopyFile 
         Height          =   285
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   4575
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1215
      Left            =   120
      TabIndex        =   36
      Top             =   3120
      Width           =   7455
      Begin VB.CheckBox Check1 
         Caption         =   "Drop filnavn"
         Enabled         =   0   'False
         Height          =   375
         Left            =   3960
         TabIndex        =   45
         Top             =   240
         Width           =   1455
      End
      Begin VB.ComboBox Combo2 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "FormEgenskaper.frx":00AC
         Left            =   120
         List            =   "FormEgenskaper.frx":00B9
         Style           =   2  'Dropdown List
         TabIndex        =   44
         Top             =   720
         Width           =   1815
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1920
         TabIndex        =   43
         Top             =   720
         Width           =   1695
      End
      Begin VB.ComboBox Combo1 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "FormEgenskaper.frx":00EE
         Left            =   4320
         List            =   "FormEgenskaper.frx":00FB
         Style           =   2  'Dropdown List
         TabIndex        =   42
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox txtDatoFormat 
         Enabled         =   0   'False
         Height          =   285
         Left            =   5760
         TabIndex        =   41
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox txtFilFilter 
         Height          =   285
         Left            =   840
         TabIndex        =   38
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtFileExt 
         Height          =   285
         Left            =   6480
         TabIndex        =   37
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Fil ext (utfil):"
         Height          =   255
         Left            =   5520
         TabIndex        =   40
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Fil-filter (innfil):"
         Height          =   375
         Left            =   120
         TabIndex        =   39
         Top             =   240
         Width           =   615
      End
   End
End
Attribute VB_Name = "FormEgenskaper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim recJobb As typTransRec
Dim recTomJobb As typTransRec

Private Sub cmdCopyFile_Click()
    GetFile Me.txtCopyFile, "copy", 3, "xml"
End Sub

Private Sub cmdRedXslt_Click()
    StartEdit recGeneral.strRootPath & "\xslt\", Me.txtTransFile
End Sub

Private Sub cmdRedXchg_Click()
    StartEdit recGeneral.strRootPath & "\xchg\", Me.txtXchgFile
End Sub

Private Sub cmdRedCopy_Click()
    StartEdit recGeneral.strRootPath & "\copy\", Me.txtCopyFile
End Sub

Private Sub cmdVisCopy_Click()
    If recJobb.lpCopyStart > 0 Then
        FormXmlCopy.Show vbModal
    End If
End Sub

Private Sub cmdVisFeil_Click()
    If recJobb.strLastError = "" Then
        MsgBox "Ingen feilmelding"
    Else
        MsgBox recJobb.strLastError
    End If
End Sub

Private Sub cmdVisXchg_Click()
    If recJobb.lpXchgStart > 0 Then
        FormXchg.Show vbModal
    End If
End Sub

Private Sub comboDelete_Validate(Cancel As Boolean)
    Dim bAktiv As Boolean
    If Me.comboDelete.Text = STR_DAYS Then
        bAktiv = True
    Else
        bAktiv = False
    End If
    Me.txtDager.Enabled = bAktiv
    Me.lblDager.Enabled = bAktiv
    
End Sub

Private Sub Form_Load()
    On Error GoTo Err_Handler
    If intJobIndex <= intTransAnt Then
        recJobb = arrTrans(intJobIndex)
    Else
        recJobb = recTomJobb
    End If
    
    With recJobb
        If .strType <> "" Then
            Me.comboJobbtype = .strType
        End If
        
        Select Case .strStatus
        Case STR_OK
            Me.chkActivate.Value = 1
        Case Else
            Me.chkActivate.Value = 0
        End Select
        
        Me.CheckDebug = .bDebug * -1
        Me.txtJobbnavn = .strText
        Me.txtStatus = .strStatus
        Me.txtInPath = .strInPath
        Me.txtOutPath = .strOutPath
        Me.txtTransFile = .strXsltFile
        Me.txtFilFilter = .strFilter
        Me.txtXchgFile = .strXchgFile
        Me.txtCopyFile = .strCopyFile
        Me.txtFileExt = .strFileExt
        If .strCopyFile = STR_DAYS Then
            Me.txtDager.Enabled = True
        End If
        Me.txtDager = .strDelDays
        
        If .strDelete = "" Then
            Me.comboDelete = STR_NO
        ElseIf .strDelete = "no" Then
            Me.comboDelete = STR_NO
        Else
            Me.comboDelete = .strDelete
        End If
        
        If .strArchive = "" Then
            Me.comboArkiv = STR_ALL
        Else
            Me.comboArkiv = .strArchive
        End If
        
    End With
    Exit Sub
Err_Handler:
    MsgBox Err.Description
    Resume Next
End Sub

Private Sub cmdOK_Click()
    If Trim(Me.txtJobbnavn) = "" Then
        MsgBox "Du m� oppgi jobbnavn!"
        Me.txtJobbnavn.SetFocus
        Exit Sub
    End If
    
    If Trim(Me.comboJobbtype) = "" Then
        MsgBox "Du m� oppgi jobbtype!"
        Me.comboJobbtype.SetFocus
        Exit Sub
    End If
    
    If Trim(Me.txtInPath) = "" Then
        MsgBox "Du m� oppgi kildemappe!"
        Me.txtInPath.SetFocus
        Exit Sub
    End If
    
    With recJobb
        .strNr = Format(intJobIndex, "00")
        .strType = Me.comboJobbtype
        .strText = Trim(Me.txtJobbnavn)
        .strInPath = Trim(Me.txtInPath)
        .strOutPath = Trim(Me.txtOutPath)
        .strXsltFile = Trim(Me.txtTransFile)
        .strXchgFile = Trim(Me.txtXchgFile)
        .strCopyFile = Trim(Me.txtCopyFile)
        .strDelDays = Trim(Me.txtDager)
        .strArchive = Me.comboArkiv
        .strFilter = Me.txtFilFilter
        .strFileExt = Me.txtFileExt
        .bDebug = Me.CheckDebug * -1
        
        Select Case Me.comboDelete.Text
        Case STR_NO
            .strDelete = ""
        Case Else
            .strDelete = Me.comboDelete
        End Select

        Select Case Me.chkActivate.Value
        Case 1
            .strStatus = STR_OK
        Case Else
            .strStatus = STR_STOPPET
        End Select
    End With
    
    If intJobIndex > intTransAnt Then
        intTransAnt = intJobIndex
        ReDim Preserve arrTrans(1 To intTransAnt)
    End If
    arrTrans(intJobIndex) = recJobb
    
    Form1.SkrivEnJobb intJobIndex
    Form1.Grid1.ColSel = 2
    Unload Me
End Sub

Private Sub cmdAvbryt_Click()
    Unload Me
End Sub

Private Sub cmdGetInPath_Click()
    HentFormFolder Me.txtInPath
End Sub

Private Sub HentFormFolder(objFelt As TextBox)
    If objFelt.Text = "" Then
        FormFolder.strInitPath = recGeneral.strRootPath
    Else
        FormFolder.strInitPath = objFelt.Text
    End If
    FormFolder.Show vbModal
    
    If FormFolder.strPath <> "" Then
        objFelt.Text = FormFolder.strPath
    End If
End Sub


Private Sub cmdGetOutPath_Click()
    HentFormFolder Me.txtOutPath
End Sub

Private Sub cmdGetXchgFile_Click()
    GetFile Me.txtXchgFile, "xchg", 2, "xml"
End Sub

Private Sub comboJobbtype_Validate(Cancel As Boolean)
    Select Case comboJobbtype
    Case "copy", "xslt"
        Me.FrameOutPath.Enabled = True
        Me.FrameXchgFile.Enabled = True
    Case "delete"
        Me.FrameOutPath.Enabled = False
        Me.FrameXchgFile.Enabled = False
        
    Case Else
    End Select
End Sub

Private Sub cmdGetTransFile_Click()
    GetFile Me.txtTransFile, "xslt", 1, "xsl"
End Sub

Private Sub GetFile(txtFileName As TextBox, strPath As String, intFilterIndex As Integer, Optional strExt As String)
    On Error GoTo Err_Handler
    Me.CommonDialog1.Filter = "XSLT-fil (*.xsl)|*.xsl|Xchg-fil (*.xml)|*.xml|Copy-fil (*.xml)|*.xml|Alle-filer (*.*)|*.*|Lyd-filer (*.mp3)|*.mp3"
    Me.CommonDialog1.FilterIndex = intFilterIndex
    Me.CommonDialog1.Flags = cdlOFNNoChangeDir Or cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNExplorer
    Me.CommonDialog1.InitDir = recGeneral.strRootPath & "\" & strPath
    If txtFileName.Text <> "" Then
        Me.CommonDialog1.FileName = Me.CommonDialog1.InitDir & "\" & txtFileName.Text
    Else
        Me.CommonDialog1.FileName = Me.CommonDialog1.InitDir & "\*." & strExt
    End If
    Me.CommonDialog1.CancelError = True
    Me.CommonDialog1.ShowOpen
    
    txtFileName.Text = GetFileBase(Me.CommonDialog1.FileName, True)
    
Exit Sub
Err_Handler:
    If Err.Number <> cdlCancel Then
        MsgBox Err.Description, vbCritical
    End If

End Sub

