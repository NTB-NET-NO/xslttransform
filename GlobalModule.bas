Attribute VB_Name = "GlobalModule"
Option Explicit

'*** Globale konstanter og variabler
Public Const MAX_TRANS = 100
Public Const MAX_XCHG = 300
Public Const MAX_COPY = 100
Public Const MAX_STILER = 500

Public Const STR_FEIL = "feil"
Public Const STR_OK = "ok"
Public Const STR_STOPPET = "stoppet"
Public Const STR_VARSEL = "varsel"
Public Const STR_YES = "ja"
Public Const STR_NO = "nei"
Public Const STR_DAYS = "dag"
Public Const STR_MODIFY = "modify"
Public Const STR_ALL = "all"
Public Const STR_NO_MOD = "no mod"

Public Const XML_HEAD = "<?xml version=""1.0"" encoding=""iso-8859-1""?>"
Public Const INI_FILE_NAME = "transform.ini"
Public Const INI_FILE_XML = "TransformIni.xml"
Public Const LOG_PATH = "log"
Public Const ERR_PATH = "error"
Public Const XSLT_PATH = "xslt"
Public Const XCHG_PATH = "xchg"
Public Const COPY_PATH = "copy"

Public varDebug As Variant
Public bIsDirty As Boolean
Public strCurDir As String

Public Glob As typGlob

Type typGlob
    bLoaded As Boolean
    Type As enuType
    'xmlDoc As DOMDocument
    strText As String
    strFileName As String
End Type

Enum enuType
    typEmpty = 0
    typText = 1
    typXmlDoc = 2
End Enum

Public recGeneral As typGeneral
Type typGeneral
    strTitle  As String
    bGui As Boolean
    bTimerEnabled As Boolean
    bTimerEnabledStart As Boolean
    strRootPath As String
    strLoggPath As String
    strSoundPath As String
    strSoundXPath As String
    strSoundXMessage As String
    intInterval As Long
    strEditor As String
End Type

Public Ini_File         As String
Public tmpDebug         As Variant
Public intTransAnt      As Integer
Public strMessage       As String
Public intJobIndex      As Integer
    
Public FSO              As Scripting.FileSystemObject
Public tstreamUt        As Scripting.TextStream
Public tstreamInn       As Scripting.TextStream
          
'Public recJobb As typTransRec
Public arrTrans() As typTransRec

Type typTransRec
    strNr               As String
    strText             As String
    strType             As String
    strXsltFile        As String
    strXchgFile         As String
    strCopyFile         As String
    strFilter           As String
    strInPath           As String
    strOutPath          As String
    strFileExt          As String
    strLogFile          As String
    strDelete           As String
    strDelDays          As String
    strArchive          As String
    strStatus           As String
    xmlDoc              As DOMDocument
    xsltProc            As IXSLProcessor
    lpXchgStart         As Long
    lpXchgEnd           As Long
    lpCopyStart         As Long
    lpCopyEnd           As Long
    bXchg               As Boolean
    bXmlXchg            As Boolean
    bLastXchg           As Boolean
    intDim              As Integer
    strErrStatus        As String
    strLastError        As String
    intAntErr           As Integer
    intAntPasErr        As Integer
    bIsDirty            As Boolean
    bDebug              As Boolean
    
    'xmlXchg             As DOMDocument
    'xmlCopy             As DOMDocument
    'intXchgLines        As Integer
End Type

Public arrXchg(1 To MAX_XCHG) As XchgRec
Type XchgRec
    strFrom     As String
    strTo       As String
    lngStart    As Long
    lngCount    As Long
    intOption   As Integer
End Type


