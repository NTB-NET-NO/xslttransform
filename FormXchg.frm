VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FormXchg 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "XCHG S�k og erstatte strenger"
   ClientHeight    =   4785
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8265
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   8265
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdLagre 
      Caption         =   "&Lagre"
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton cmdNy 
      Caption         =   "&Ny"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton cmdSlett 
      Caption         =   "&Slett"
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5520
      TabIndex        =   4
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton cmdAvbryt 
      Cancel          =   -1  'True
      Caption         =   "Avbryt"
      Height          =   375
      Left            =   6960
      TabIndex        =   5
      Top             =   4320
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid Grid1 
      Height          =   4095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8055
      _ExtentX        =   14208
      _ExtentY        =   7223
      _Version        =   393216
      Cols            =   4
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "FormXchg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lpStart As Long

Private Sub cmdAvbryt_Click()
    Unload Me
End Sub

Private Sub cmdLagre_Click()
    SaveXchg
End Sub

Private Sub cmdNy_Click()
    FormReplace.lpXchgIndex = 0
    FormReplace.intRow = Me.Grid1.Rows
    FormReplace.Show vbModal
End Sub

Private Sub cmdOK_Click()
    '*** sett inn kode her
    Unload Me
End Sub

Private Sub cmdSlett_Click()
    Dim strMelding As String
    Dim lpIndex As Long
    Dim lpNext As Long
    Dim lpPrev As Long
    Dim intRow As Integer
    
    Me.Grid1.Col = 0
    strMelding = strMelding & "Nr.: " & Me.Grid1.Text
    Me.Grid1.Col = 1
    strMelding = strMelding & ", Type: " & Me.Grid1.Text
    Me.Grid1.Col = 2
    strMelding = strMelding & ": " & Me.Grid1.Text
    
    If MsgBox("Vil du slette " & strMelding, vbYesNo) = vbNo Then
        Exit Sub
    End If
    lpIndex = Me.Grid1.RowData(Me.Grid1.Row)
    With arrXmlXchg(lpIndex)
        lpPrev = .lpPrevNode
        lpNext = .lpNexNode
        
        If .lpPrevNode = 0 Then
            arrTrans(intJobIndex).lpXchgStart = .lpNexNode
        Else
            arrXmlXchg(.lpPrevNode).lpNexNode = .lpNexNode
        End If
        
        If .lpNexNode = 0 Then
            arrTrans(intJobIndex).lpXchgEnd = .lpPrevNode
        Else
            arrXmlXchg(.lpNexNode).lpPrevNode = .lpPrevNode
        End If
        
    End With
    
    Me.Grid1.RemoveItem (Me.Grid1.Row)
        
End Sub

Private Sub Form_Load()
    Dim lpEnd As Long
    Dim intRow As Integer
    Dim lpIndex As Long
    
    With arrTrans(intJobIndex)
        lpStart = .lpXchgStart
        lpEnd = .lpXchgEnd
    End With
        
    Me.Grid1.Row = 0
    Me.Grid1.ColWidth(0) = 300
    Me.Grid1.ColWidth(1) = 500
    Me.Grid1.ColWidth(2) = 5000
    Me.Grid1.ColWidth(3) = 1000
    
    Me.Grid1.Col = 0
    Me.Grid1.Text = "Nr"
    Me.Grid1.Col = 1
    Me.Grid1.Text = "Type"
    Me.Grid1.Col = 2
    Me.Grid1.Text = "XPath"
    Me.Grid1.Col = 3
    Me.Grid1.Text = "# replace"
    
    If lpStart = 0 Then
        'cmdNy_Click
        Exit Sub
    End If
    
    lpIndex = lpStart
    Do
        intRow = intRow + 1
        VisEnRad intRow, lpIndex
        
    Loop Until lpIndex = 0
    
    Me.Grid1.Row = 1
    Me.Grid1.Col = 1
    Me.Grid1.ColSel = 3
End Sub
       
Sub VisEnRad(intRow As Integer, lpIndex As Long)
    If intRow > Me.Grid1.Rows - 1 Then
        Me.Grid1.AddItem ("")
    End If
    Me.Grid1.Row = intRow
        
    With arrXmlXchg(lpIndex)
        Me.Grid1.RowData(intRow) = lpIndex
        Me.Grid1.Col = 0
        Me.Grid1.Text = Format(intRow, "00")
        Me.Grid1.Col = 1
        Me.Grid1.Text = .strType
        Me.Grid1.Col = 2
        Me.Grid1.Text = .strXpath
        Me.Grid1.Col = 3
        Me.Grid1.Text = .lpReplaceEnd - .lpReplaceStart + 1
        lpIndex = .lpNexNode
    End With
End Sub

Private Sub Grid1_dblClick()
    'FormReplace.lpXchgIndex = lpStart + Me.Grid1.Row - 1
    'Me.Grid1.Col = 1
    FormReplace.lpXchgIndex = Me.Grid1.RowData(Me.Grid1.Row) 'Me.Grid1.Text
    FormReplace.intRow = Me.Grid1.Row
    'Me.Grid1.ColSel = 4
    FormReplace.Show vbModal
    'Me.Grid1.Col = 1
    'Me.Grid1.ColSel = 4
End Sub

Private Sub Grid1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Grid1_dblClick
    End If
End Sub

Sub SaveXchg()
    Dim xmlDoc As DOMDocument
    Dim lpIndex As Long
    Dim strXml As String
    
    strXml = XML_HEAD & vbCrLf
    strXml = strXml & LagXmlTag("xchg", "", , 1)
    lpIndex = lpStart
    
    Do
        With arrXmlXchg(lpIndex)

            strXml = strXml & LagXmlTag(.strType, "", "string=" & .strXpath, 1, .bCData)
            strXml = strXml & SaveReplace(.lpReplaceStart, .strType)
            strXml = strXml & LagXmlTag(.strType, "", , 2)

            lpIndex = .lpNexNode
        End With
    Loop Until lpIndex = 0
    
    strXml = strXml & LagXmlTag("xchg", "", , 2)

    'InputBox "", "", strXml
    
    If Not LoadXmlDoc(xmlDoc, strXml, False, strMessage, True) Then
        MsgBox strMessage
        Debug.Print strXml
        Exit Sub
    End If
        
    xmlDoc.save recGeneral.strRootPath & "\xchg\" & arrTrans(intJobIndex).strXchgFile
    '"c:\testXCHGsave.xml"
    Set xmlDoc = Nothing
    
    'SaveFile "c:\testXCHGsave.xml", strXml
    
End Sub

Private Function SaveReplace(lpStartReplace As Long, strType As String) As String
    Dim lpIndex As Long
    Dim strXml As String
    
    lpIndex = lpStartReplace
    Do
            
        With arrXchgReplace(lpIndex)
                    
            strXml = strXml & LagXmlTag("replace", "", "start=" & .intStart & ";count=" & .intCount & ";compare=" & .intCompare, 1)
            strXml = strXml & LagXmlTag("f", Replace(.strFindShow, vbCrLf, STR_CRLF), "ent=" & .bFindEnt, 0, True)
            strXml = strXml & LagXmlTag("r", Replace(.strReplaceShow, vbCrLf, STR_CRLF), "ent=" & .bReplaceEnt, 0, True)
            Select Case strType
            Case "bulk"
                strXml = strXml & LagXmlTag("f2", Replace(.strFind2Show, vbCrLf, STR_CRLF), "ent=" & .bFind2Ent, 0, True)
            Case "pair"
                strXml = strXml & LagXmlTag("f2", Replace(.strFind2Show, vbCrLf, STR_CRLF), "ent=" & .bFind2Ent, 0, True)
                strXml = strXml & LagXmlTag("r2", Replace(.strReplace2Show, vbCrLf, STR_CRLF), "ent=" & .bReplace2Ent, 0, True)
            End Select
            strXml = strXml & LagXmlTag("replace", "", , 2)
            
            lpIndex = .lpNexNode
        End With
        
    Loop Until lpIndex = 0

    SaveReplace = strXml
End Function

